\chapter{Signalverarbeitungsgrundlagen}
\label{sec:SVG}
In diesem Kapitel werden die notwendigen Grundlagen der Signalverarbeitung vorgestellt.
Die verschiedenen Verfahren der Fourier-Transformation und der Bestimmung der Amplitude einzelner Frequenzen werden in Abschnitt \ref{sec:Fourier-Transformation} behandelt.
Die Notwendigkeit von Fenstern bei der Amplitudenbestimmung erklärt Abschnitt \ref{sec:Fenster}.

\section{Fourier-Transformation}
\label{sec:Fourier-Transformation}
Nichtperiodische, zeitabhängige Funktionen $f\left(t\right),-\infty <t<\infty$, wie sie zum Beispiel bei Spannungsverläufe auftreten, lassen sich mit der Fourier-Transformation in komplexe, harmonische Schwingungen umwandeln.
Die Umwandlung der Funktion erfolgt dabei gemäß Gleichung \eqref{eq:FT}.
$F(\omega)$ bezeichnet dabei die Fourier-Transformierte von $f(t)$.
Die Transformation ist nur möglich, wenn das uneigentliche Integral existiert(\cite[S. 541]{Papula2015}).

\begin{equation} \label{eq:FT}
F\left(\omega \right)=\int_{-\infty }^{\infty } f\left(t\right)e^{-j\omega t} \text{dt}
\end{equation}

Der in Abbildung \ref{fig:Zeitbereich Rechteckimpuls} dargestellte Rechteckimpuls
$f(t)=\begin{cases}
1, & \text{wenn} |t|\le a\\
0, & \text{wenn} |t|>a
\end{cases}$
wird in Gleichung \eqref{eq:FT von Rechteck} fourier-transformiert.
Die Fourier-Transformierte des Rechtecksignals liefert für den Fall $\omega = 0$ kein Ergebnis.
Daher wird der Wert für diesen Spezialfall gemäß Gleichung \eqref{eq:FT von Rechteck Gleichanteil} separat berechnet.
\begin{equation} \label{eq:FT von Rechteck}
F(\omega)=\int_{-\frac{T}{2}}^\frac{T}{2} 1\cdot e^{-j\omega t} \text{dt}={\left[ \frac{e^{-j\omega t} }{-j\omega t}\right] }_{-\frac{T}{2}}^\frac{T}{2} =-\frac{1}{j\omega }\left(e^{\frac{-j\omega T}{2} } -e^{j\frac{j\omega T}{2}} \right)=\frac{2\cdot \sin \left(\frac{T\omega}{2}\right)}{\omega }
\end{equation}
\begin{equation}\label{eq:FT von Rechteck Gleichanteil}
F\left(\omega =0\right)=\int_{-\frac{T}{2}}^\frac{T}{2} 1\cdot e^{-\text{j0t}} \text{dt}=\int_{-\frac{T}{2}}^\frac{T}{2} 1\text{ }\text{dt}=T
\end{equation}

\begin{figure}
	\centering
	\subcaptionbox{\label{fig:Zeitbereich Rechteckimpuls}}
	[.49\textwidth]{\includegraphics{Abbildungen/fourier_transformation_rechteck_zeit.eps}}
	\subcaptionbox{\label{fig:Bildbereich Rechteckimpuls}}
	[.49\textwidth]{\includegraphics{Abbildungen/fourier_transformation_rechteck_bild.eps}}
	\caption[Rechteckimpuls im Zeit- und Bildbereich]
	{\subref{fig:Zeitbereich Rechteckimpuls} zeigt einen Rechteckimpuls im Zeitbereich.
	Innerhalb des Bereichs von $t=-\frac{T}{2}$ bis $\frac{T}{2}$ hat der Impuls den Wert 1, sonst 0.
	
	\subref{fig:Bildbereich Rechteckimpuls} zeigt den Rechteckimpuls im Bildbereich.
	Es entspricht der Sinc-Funktion $\frac {\sin \omega}{\omega}$.
	Zum Zeitpunkt $\omega$ = \SI{0}{\per\second} beträgt der Wert des Signals $T$ und entspricht damit dem Integral des Zeitbereichs.}
	\label{fig:FT eines Rechteckimpulses}
\end{figure}

Für die Fourier-Transformation gelten mehrere Transformationssätze.
Unter anderem der Vertauschungssatz (Gleichung \eqref{eq:FT Vertauschungssatz}) \cite[S. 587]{Papula2015} und der Faltungssatz (Gleichung \eqref{eq:FT Faltungssatz}) \cite[S. 583]{Papula2015}.
\begin{equation} \label{eq:FT Vertauschungssatz}
F(t) \laplace 2\pi f(\omega)
\end{equation}
\begin{equation} \label{eq:FT Faltungssatz}
f_1(t) \ast f_2(t) \laplace F_1(\omega)\cdot F_2(\omega)
\end{equation}
Durch diese Sätze lassen sich auch die periodischen Funktionen $f(t) = \sin t$ und $f(t) = \cos t$, die sich durch die ursprüngliche Gleichung \eqref{eq:FT} nicht transformieren lassen, umwandeln.
Es gelten die Gleichungen \eqref{eq:FT Sinus} und \eqref{eq:FT Cosinus} nach \cite[S. 591]{Papula2015}.
\begin{equation}\label{eq:FT Sinus}
\sin(\omega_0\cdot t) \laplace j\pi\left[\delta(\omega+\omega_0)-\delta(\omega-\omega_0)\right]
\end{equation}
\begin{equation} \label{eq:FT Cosinus}
\cos(\omega_0\cdot t)\laplace\pi\left[\delta(\omega+\omega_0)+\delta(\omega-\omega_0)\right]
\end{equation}
In der späteren, praktischen Anwendung können keine unendlich langen Signale betrachtet werden, sondern nur Ausschnitte.
In Gleichung \eqref{eq:gefensterter Sinus} wird ein Signalabschnitt für einen Sinus formuliert.
Das Ausschneiden von Funktionsbereichen wird auch als Fensterung bezeichnet. 
Die Berechnung der Fourier-Transformierten erfolgt in Gleichung \eqref{eq:Fourier, Sinus}.
\begin{equation}\label{eq:gefensterter Sinus}
f(t)=\begin{cases}
\sin \frac{2\pi t}{T}, & \text{wenn} |t|\le a\\
0, & \text{wenn} |t|>a
\end{cases}
\end{equation}
\begin{align} \label{eq:Fourier, Sinus}
F\left(\omega \right)&=\int_{-\frac{T}{2} }^{\frac{T}{2} } \sin\left(\frac{2\pi t}{T}\right) e^{-j\omega t} \text{dt} \\
&=\left[\frac{T e^{\frac{-j\omega T}{2}}}{T^2\omega^2-4\pi^2}\left(2\pi\cos\left(\frac{2\pi t}{T}\right)+j\omega T\sin\left(\frac{2\pi t}{T}\right)\right)\right]_{-\frac{T}{2}}^{\frac{T}{2}} \nonumber \\
&=\frac{j4\pi T}{T^2\omega^2-4\pi^2} \sin \frac{\omega T}{2} \nonumber
\end{align}
Die Abbildung \ref{fig:Zeitbereich Fenstersinus} zeigt den im Bereich $-\frac{T}{2}$ und $\frac{T}{2}$ Sinus im Zeitbereich und Abbildung \ref{fig:Bildbereich Fenstersinus} im Bildbereich.
Auffällig ist, dass das Maximum bzw. Minimum der Funktionen kein Vielfaches von $\omega$ ist.

Der Grund lässt sich über den alternativen Lösungsweg für die Transformation feststellen.
Durch Gleichung \eqref{eq:FT Sinus} ist bekannt, dass der Sinus im Bildbereich aus zwei Diracstößen besteht.
Ebenso ist bekannt, das ein Rechteckimpuls im Bildbereich Gleichung \eqref{eq:FT von Rechteck} entspricht.
Zusammen mit dem Faltungssatz aus Gleichung \eqref{eq:FT Faltungssatz} und dem Vertauschungssatz aus Gleichung \eqref{eq:FT Vertauschungssatz} und dem Wissen, dass eine Faltung mit einem Dirac-Stoß einer Verschiebung entspricht, ergibt sich Gleichung \eqref{eq:Alternativ, Fenstersinus}.
Teil A entspricht dabei der blau gestrichelten Linie in Abbildung \ref{fig:Bildbereich Fenstersinus}, während Teil B der roten gestrichelten Linie entspricht.
Wird Teil A von Teil B subtrahiert, entsteht die Funktion $F(\omega)$.
\begin{equation}\label{eq:Alternativ, Fenstersinus}
F(\omega) = j\left[
\underbrace{\frac{\sin \left(\frac{\omega T}{2}+\pi T^2\right)}
{\omega+2\pi T}}_{\text{A}}
-
\underbrace{\frac{\sin \left(\frac{\omega T}{2}-\pi T^2\right)}
{\omega-2\pi T}}_{\text{B}}
\right]
\end{equation}

\begin{figure}
	\centering
	\subcaptionbox{\label{fig:Zeitbereich Fenstersinus}}
	[.49\textwidth]{\includegraphics{Abbildungen/fourier_transformation_sinus_rechteck_zeit.eps}}
	\subcaptionbox{\label{fig:Bildbereich Fenstersinus}}
	[.49\textwidth]{\includegraphics{Abbildungen/fourier_transformation_sinus_rechteck_bild.eps}}
	\caption[Gefensterter Sinus im Zeit- und Bildbereich]
	{\subref{fig:Zeitbereich Fenstersinus} zeigt den gefensterten Sinus gemäß Gleichung \eqref{eq:gefensterter Sinus}.
	
	\subref{fig:Bildbereich Fenstersinus} zeigt die Fourier-transformierte des gefensterten Sinus gemäß Gleichung \eqref{eq:Fourier, Sinus}
	Zusätzlich ist Teil A (blau) und Teil B (rot) des alternativen Berechnungsweges nach Gleichung \eqref{eq:Alternativ, Fenstersinus} eingezeichnet.
	}
	\label{fig:FT eines Sinusrechtecks}
\end{figure}

\subsection{Diskrete Fourier-Transformation}
\label{sec:DFT}
Im Rahmen dieser Arbeit werden zeitdiskrete Werte verarbeitet.
Aus diesem Grund muss auch die Fourier-Transformation zeitdiskret erfolgen.

Die Erklärungen innerhalb dieses Abschnittes folgen dem Lehrbuch von Andreas Wendemuth \cite[S. 151-161]{Wendemuth2004}.
Die zeitdiskrete Fourier-Transformation ist in Gleichung \eqref{eq:FT, vollständig diskret} definiert.
Die Integration wurde durch eine Summation ersetzt.
$t$ wurde durch $kT$ substituiert, $\omega$ durch  $n\frac{2\pi}{NT}$.
$T$ entspricht der Abtastzeit und ist der Kehrwert der Abtastfrequenz $f_a= \frac{1}{T}$. $N$ ist die Anzahl der Datenpunkte.

\begin{equation}\label{eq:FT, vollständig diskret}
X\left(n\right)=\sum_{-\infty }^{\infty } x\left\lbrack k\right\rbrack e^{-\text{jn}\frac{2\pi }{N}k}
\end{equation}

Zur Vereinfachung wird der komplexe Drehoperator $W_N =e^{-j\frac{2\pi}{N}}$ eingeführt.
Dieser durchläuft den Einheitskreis periodisch mit der Grundperiode $N$.
Die Periodizität wird genutzt, um die unendliche Summe aus Gleichung \eqref{eq:FT, vollständig diskret} gemäß Gleichung \eqref{eq:DFT, Blockbildung} in Blöcke von je $N$ Summanden zu teilen.

\begin{align}\label{eq:DFT, Blockbildung}
X\left(n\right) &=\sum_{-\infty }^{\infty } x\left[ k\right] W_N^{\text{nk}} \\
&=\dots +\sum_{k=0}^{N-1} x\left[ k\right] W_N^{\text{nk}} +\sum_{k=N}^{2N-1} x\left[ k\right] W_N^{\text{nk}} +\dots \nonumber \\
&=\text{ }\dots +\sum_{k=0}^{N-1} x\left[ k\right] W_N^{\text{nk}} +\sum_{k=0}^{N-1} x\left[ k+N\right] W_N^{n\left(k+N\right)} +\dots \nonumber
\end{align}

Durch die Periodizität von $W_N$ lässt sich Gleichung \eqref{eq:DFT, Blockbildung} auch gemäß Gleichung \eqref{eq:DFT, periodisch} schreiben.

\begin{equation}\label{eq:DFT, periodisch}
X\left(n\right)=\sum_{k=0}^{N-1} \left\lbrack \sum_{r=-\infty }^{\infty } x\left\lbrack k+\text{rN}\right\rbrack \right\rbrack W_N^{\text{nk}}
\end{equation}

Wird nun $\tilde{x} \left\lbrack k\right\rbrack$ nach Gleichung \eqref{eq:DFT, x_tilde} definiert, ergibt sich die Fourier-Transformationsgleichung \eqref{eq:DFT mit x_tilde}.

\begin{equation}\label{eq:DFT, x_tilde}
\tilde{x} \left\lbrack k\right\rbrack =\sum_{r=-\infty }^{\infty } x\left\lbrack k+\text{rN}\right\rbrack
\end{equation}

\begin{equation}\label{eq:DFT mit x_tilde}
X\left(n\right)=\sum_{k=0}^{N-1} \tilde{x} \left\lbrack k\right\rbrack W_N^{\text{nk}}
\end{equation}

Solange die Folge $x[k]$ weniger oder gleich viele $N$ aufeinander folgende Glieder hat, gilt $x[k] = \tilde{x} [k]$.
Da der komplexe Drehoperator $W_N$ periodisch ist, setzt sich $X(n)$ periodisch in beide Richtungen fort. \cite[S. 152]{Wendemuth2004}

Die bereits bekannte, mit einer Rechteckfunktion multiplizierte, Sinusfunktion gemäß Gleichung \eqref{eq:gefensterter Sinus} wird gemäß Gleichung \eqref{eq:DFT, Sinus} einer diskreten Fourier-Transformation unterzogen. Für $N=16$ Werte im Zeitbereich ergeben sich auch 16 Werte im Bildberech. Die Abtastzeit wird auf $T_a=\frac{1}{N} = \SI{62.5}{\milli\second}$ gesetzt.
\begin{equation} \label{eq:DFT, Sinus}
x(k) = \sin \left(2\pi k\cdot T_a\right) \laplace X(n) = \sum_{k=\frac{-N}{2}+1}^{\frac{N}{2}} \sin \left(2\pi k T_a\right) \cdot e^{\frac{-j2\pi kn}{N}}
\end{equation}
In Abbildung \ref{fig:DFT Sinus Zeit} sind die abgetasten Werte im Zeitbereich und in Abbildung \ref{fig:DFT Sinus Bild} im Bildbereich dargestellt.
Die diskrete Fourier-Tansformation ist an allen Stellen außer $n=\pm1$ null.
An den Stellen $n=\pm1$ beträgt der Wert $4$ bzw. $-4$.
Werden alle Werte der diskreten Fourier-Transformation durch die Länge $N$ geteilt, liegen sie auf den Werten der bereits zuvor durchgeführten zeit-kontinuierlichen Fourier-Transformation.
Diese ist in Abbildung \ref{fig:DFT Sinus Bild} zusätzlich rot gestrichelt eingezeichnet.
Gleichzeitig lässt sich nun auch die Amplitude des Signals im Zeitbereich ermitteln.
Dazu wird zuerst der Betrag gebildet und anschließend um den Nullpunkt von $n$ gespiegelt und summiert.
Anschließend wird durch die Länge $N$ der Fourier-Transformation dividiert.
Somit ergibt sich für $n=1$ eine Amplitude von $1$.

\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:DFT Sinus Zeit}}
	[.49\textwidth]{\includegraphics{Abbildungen/dft_sinus_zeit.eps}}
	\subcaptionbox{\label{fig:DFT Sinus Bild}}
	[.49\textwidth]{\includegraphics{Abbildungen/dft_sinus_bild.eps}}
	\caption[Diskrete Fourier-Transformation eines Sinuses im Zeit- und Bildbereich]{
	\subref{fig:DFT Sinus Zeit} zeigt die Werte der diskreten Sinusfunktion  $x(k)$ nach Gleichung \eqref{eq:DFT, Sinus}
	
	\subref{fig:DFT Sinus Bild} zeigt die Werte der diskret fourier-transformierte $X(n)$ nach Gleichung \eqref{eq:DFT, Sinus}.
	Zusätzlich ist der bereits bekannte Verlauf der fourier-transformierten nach Gleichung \eqref{eq:Fourier, Sinus} in rot eingezeichnet.
	}
	\label{fig:DFT Sinus}
\end{figure}

Die Abstände der diskreten Frequenzen betragen $\frac{1}{T_A}$.
So lässt sich die Frequenz für alle $n$ nach Gleichung \eqref{eq:DFT, Frequenz} bestimmen.

\begin{equation} \label{eq:DFT, Frequenz}
f(n) = \frac{n}{T_a}
\end{equation}

Offenbar enthält das Sinussignal die Frequenzen $f(n=-1)=\frac{1}{\SI{62.5}{\milli\second}}=\SI{-16}{\hertz}$ und $f(n=1) = \SI{16}{\hertz}$.
An diesen Stellen ist die Amplitude $-\frac{jN}{2}$ bzw. $\frac{jN}{2}$.
Mit der inversen diskreten Fourier-Transformation gemäß Gleichung \eqref{eq:IDFT} lässt sich die Fourier-Transformation wieder aufheben und der diskrete Verlauf im Zeitbereich ermitteln.

\begin{equation} \label{eq:IDFT}
x(k) = \frac{1}{N} \sum_{k=0}^{N-1} X(n) W_N^{-nk} = \frac{1}{N} \sum_{k=0}^{N-1} X(n) e^{j\frac{2\pi}{N}kn}
\end{equation}

Für das Beispiel aus Gleichung \eqref{eq:DFT, Sinus} wird die Ermittlung im Zeitbereich in Gleichung \eqref{eq:IDFT_Sinus} gezeigt.
Statt der Berechnung kann auch die Punktsymmetrie ausgenutzt werden.
Die Werte aller $n>0$ werden verdoppelt und die Werte $n<0$ fallen weg.
\begin{align} \label{eq:IDFT_Sinus}
x(k) &= \frac{1}{N} \left(X(-1)\cdot e^{-j\frac{2\pi}{N}k}+X(1)\cdot e^{j\frac{2\pi}{N}k} \right) \\ \nonumber
&= \frac{1}{N}\left(-\frac{jN}{2}\cdot e^{-j\frac{2\pi}{N}k}+\frac{jN}{2}\cdot e^{j\frac{2\pi}{N}k} \right) \\ \nonumber
&= \frac{jN}{2N}\left(-e^{-j\frac{2\pi}{N}k}+e^{j\frac{2\pi}{N}k}\right) \\ \nonumber
&=\frac{j}{2}\left(
\underbrace{
\cos\left(-\frac{2\pi}{N}k\right)
-\cos\left( \frac{2\pi}{N}k\right)}_{0}
+
\underbrace{
j\sin\left(-\frac{2\pi}{N}k\right)
-j\sin\left( \frac{2\pi}{N}k\right)}_{-j2\sin\left( \frac{2\pi}{N}k\right)}
\right) \\ \nonumber
&= \sin\left(\frac{2\pi}{N}k\right) = \sin\left(2\pi k \cdot T_a\right)
\end{align}

\subsection{Schnelle Fourier-Transformation}
\label{sec:FFT}
Die diskrete Fourier-Transformation erfordert für alle $N$ Punkte $N$ Berechnungen.
Der Aufwand steigt somit quadratisch mit den zu berechnenden Datenpunkten an.
Es gibt verschiedene Ansätze, die Komplexität zu minimieren.
Diese sind unter dem Begriff schnelle Fourier-Transformation (\acs{FFT}, \acl{FFT}) bekannt.
Neben dem hier vorgestellten Verfahren für Fourier-Transformationen mit der Länge einer beliebigen Zweierpotenz gibt es aufwendige Algorithmen, wie \cite{Bluestein_1970}.
Sie können schnelle Fourier-Transformationen mit beliebiger Länge durchführen oder sind speziell auf bestimmte Hardware zugeschnitten.
So bietet die Firma Xilinx für das in dieser Arbeit verwendete \ac{SoC} einen \ac{IP-Core} an, der eine schnelle Fourier-Transformation durchführt.
Wie dieser Berechnungen durchführt, ist jedoch nicht öffentlich dokumentiert.

Daher wird hier der einfachste Fall, eine schnelle Fourier-Transformation mit der Länge einer Zweierpotenz, demonstriert.
Dazu wird die bekannte diskrete Fourier-Transformationsgleichung gemäß Gleichung \eqref{eq:FFT, Split} in zwei Summen aufgeteilt.
Für alle geraden $k$ wird $k$ durch $2l$ und alle ungraden $k$ werden durch $l+1$ substituiert.
Daraus ergibt sich Gleichung \eqref{eq:FFT, Split}.

\begin{equation} \label{eq:FFT, Split}
X\left(n\right)= \sum_{k=0}^{N-1} x \left[ k\right] W_N^{\text{nk}} = 
\sum_{l=0}^{M-1} x \left[ 2l\right] W_N^{\text{2nl}} + 
\sum_{l=0}^{M-1} x \left[ 2(l+1)\right] W_N^{\text{(2n+1)l}}
\end{equation}

Da $W_N^{\text{nk}}$ periodisch ist, kann $W_N^{\text{2nl}}$ durch $W_N^{\text{nl}}$ substituiert werden.
Ebenfalls kann $W_N^{\text{(2n+1)l}}$ auch als $W_N^{\text{l}}W_N^{\text{nl}}$ geschrieben werden.
Daraus ergibt sich die Gleichung \eqref{eq:FFT, halbe Länge}.
Der Berechnungsaufwand beträgt nun $\frac{N}{2}^2$.

\begin{equation} \label{eq:FFT, halbe Länge}
X\left(n\right)=
\sum_{l=0}^{M-1} x \left[ 2l\right] W_N^{\text{nl}} + 
W_N^{\text{n}} \sum_{l=0}^{M-1} x \left[ 2(l+1)\right] W_N^{\text{nl}}
\end{equation}

Die Aufteilung kann allerdings wiederholt werden, bis nur noch Transformationen der Länge 2 durchgeführt werden müssen.
Gleichung \eqref{eq:FFT, 2er} zeigt eine dieser Transformationen, die Gleichungen \eqref{eq:FFT, 2er, n=0} und \eqref{eq:FFT, 2er, n=1} die entsprechenden Lösungen.

\begin{equation} \label{eq:FFT, 2er}
X\left(n\right)= \sum_{k=0}^{1} x \left[ k\right] W_2^{\text{nk}}
\end{equation}
\begin{equation} \label{eq:FFT, 2er, n=0}
X\left(n = 0\right)= x(0)+x(1)
\end{equation}
\begin{equation} \label{eq:FFT, 2er, n=1}
X\left(n = 1\right)= x(0)-x(1)
\end{equation}

Diese Operation wird in der Literatur als Butterfly-Operation bezeichnet, da sie, aufgezeichnet als Signalflußgraph, wie in Abbidlung \ref{fig:Butterfly} an einen Schmetterling erinnert.\cite[S. 176]{Wendemuth2004}

\begin{figure}[ht]
	\center
	\includegraphics{Abbildungen/Butterfly_aus_Wendemuth2004.eps}
	\caption[Signalflußgraph der schnellen Fourier-Transformation Basisopertion (Butterfly)]{Signalflußgraph der FFT Basisopertion. Diese Graph ist auch unter dem Namen Butterfly bekannt.
	(aus \cite[S. 176]{Wendemuth2004})
}
	\label{fig:Butterfly}
\end{figure}

In Abbildung \ref{fig:FFT-Butterfly} ist der vollständige Signalflußgraph einer FFT für die Länge $N=8$ aufgezeichnet.
Er veranschaulicht, wie eine Fourier-Transformation durch Gleichung \eqref{eq:FFT, halbe Länge} mehrfach aufgeteilt wird und schließlich in einfachen Butterfly-Operationen mündet.
Statt ursprünglich $N^2$ Berechnungen werden nun nur noch $\log_2 N$ Berechnungen benötigt.
Für den gezeigten Fall $N=8$ bedeutet dies eine Einsparung von 62.5\%.
Für den Fall $N=2048$ werden 99,5\% aller Berechnungen eingespart.

\begin{figure}[ht]
	\center
	\includegraphics{Abbildungen/FFT8_Butterfly_aus_Wendemuth2004.eps}
	\caption[Signalflußgraph einer schnellen Fourier-Transformation der Länge 8]{
	Signalflußgraph einer FFT der Länge $N=8$.
	Zur besseren Übersicht wurden die Eingangswerte $f(n)$ anders angeordnet.
	Signale fließen von links nach rechts.
	Die Kanten sind mit dem Faktor 1 gewichtet, außer sie sind anderweitig beschrieben.
	Jeder Knoten setzt sich aus der Summation der in ihn hineinfließenden Kanten zusammen.
	(aus \cite[S. 176]{Wendemuth2004})
}
	\label{fig:FFT-Butterfly}
\end{figure}

\section{Fenster}
\label{sec:Fenster}
Bei den bisherigen diskreten Fourier-Transformationen wurden die Parameter des Sinussignals immer so gewählt, dass die Fourier-Transformation über ein ganzzahliges Vielfaches einer Periode gebildet wurde.
Dazu wurde ein Sinus wie in Abschnitt \ref{sec:DFT} gezeigt mit einer Rechteckfunktion ausgeschnitten.
Innerhalb dieser Arbeit kann das Kriterium "`ganzzahliges Vielfaches"' jedoch nicht erfüllt werden, da die exakte Frequenz des Sinussignals unbekannt ist und die Längenvorgabe der FFT fest ist.

Abbildung \ref{fig:Fenster_Problem} zeigt, dass es bei der Verletzung dieses Kriteriums zu sogenannten Leckeffekten kommt.
Hier wurde ein Sinussignal mit der Amplitude 1 und der Frequenz \SI{100}{\hertz} mit einem Rechteckimpuls von \SI{100}{\milli\second} gefenstert. Auch das Umklappen und Teilen durch die Länge der Transformation wurde durchgeführt.
Das Abtasten erfolgte mit einer Frequenz von \SI{1}{\kilo\hertz}.
Blau zeigt den Betrag der zeitkontinuierlichen Fourier-Transformation.
Rot die Werte der DFT für den Fall $N=100$, also einem ganzzahligen Vielfachen der Periodendauer.
Gelb hingegen steht für den Fall $N=128$, welches kein ganzzahliges Vielfaches der Periodendauer ist.
Es fällt auf, dass alle diskret berechneten Punkte auf der zeitkontinuierlichen Kurve liegen.
Die diskreten Werte für den Fall $N=100$ liegen auf den Nulldurchgängen und am Maximum der zeitkontinuierlichen Kurve.
Leicht lässt sich die Frequenz von \SI{100}{\hertz} und die Amplitude von 1 des fourier-transformierten Signals ablesen.
Für den Fall $N=128$ ist dies nicht der Fall.
Aufgrund der Werte lassen sich keine Rückschlüsse auf die Amplitude des Sinussignals ziehen und man erhält den Eindruck, das ursprüngliche Signal sei aus verschiedenen Frequenzen zusammengesetzt.

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{Abbildungen/fenster_rechteck_problem.eps}
	\caption[Diskrete Fourier-Transformation mit unterschiedlichen Längen]{
	Betrag der Amplitude der Fourier-Transformation eines Sinuses mit einer Frequenz von \SI{100}{\hertz}. Der Sinus wurde einem Rechteckfenster von \SI{100}{\milli\second} multipliziert.
	Blau zeigt den zeitkontinuierlichen Verlauf. Rot zeigt die Werte einer DFT der Länge 100 und gelb die Werte einer DFT der Länge 128. Die Abtastung erfolgte bei beiden DFT mit \SI{1}{\kilo\hertz}.
	}
	\label{fig:Fenster_Problem}
\end{figure}

Die Funktion, mit der eine andere Funktion ausgeschnitten wird, wird auch Fensterfunktion oder einfach nur Fenster genannt.
Statt einer Rechteckfunktion bzw. eines Rechteckfensters können auch andere Funktionen als Fenster genutzt werden.
Es gibt eine Vielzahl an Fenstern, die sich in Frequenzauflösung, Stabilität, Leck-Effekten und Glätte unterscheiden.
Häufig werden Fenster über ein Cosinus-Schema nach Gleichung \eqref{eq:Fenster, Cosinus-Schema} berechnet.
In Tabelle \ref{tab:Fensterkoeffizenten} sind bekannte Fenster und ihre Koeffizienten aufgelistet.
Weitere Methoden zur Berechnung von Fenstern finden sich in \cite{Prabhu2013} oder \cite[S. 711 ff.]{Budge2015}.

\begin{align}\label{eq:Fenster, Cosinus-Schema}
w(k)= \sum_{i=0} a_i \cos \left( \frac{2\pi ki}{N-1}\right) & 0\le k\le N-1
\end{align}
\begin{table}[hb]
	\centering
		\begin{tabular}{|l|r|r|r|r|r|}
			\hline
			Fensterbezeichnung											& $a_0$ 			& $a_1$ 			& $a_2$ 			& $a_3$ 				& $a_4$				\\
			\hline
			von Hann \cite[S. 273]{Blackman1958}				& 0,5					& -0,5 				& 0 					& 0							& 0 					\\
			\hline
			Hamming	\cite[S. 273]{Blackman1958}				& 0,54				& -0,46 			& 0 					& 0 						& 0 					\\
			\hline
			Flattop \cite[S. 70]{DAntona2006}					& 0,215579	& -0,416632	& 0,2772632	& -0,0835789	& 0,0069474	\\
			\hline
			Blackmann \cite[S. 63]{Harris1978}				& 0,42				& -0,5				& 0.08 				& 0 						& 0 					\\
			\hline
			Blackmann-Harris \cite[S. 65]{Harris1978}	& 0,35875			& -0,48829			& 0,14128			& -0,01168 			& 0 					\\
			\hline
		\end{tabular}
	\caption[Fensterfunktionen]{
	Eine Auswahl an Fensterkoeffizenten zur Berechnung von Fenstern gemäß dem Cosinus-Schema nach Gleichung \eqref{eq:Fenster, Cosinus-Schema}.
	Weitere Fenster finden sich in der Literatur \cite{Prabhu2013} oder \cite[S. 711 ff.]{Budge2015}.
	}
	\label{tab:Fensterkoeffizenten}
\end{table}

Wenn ein Fenster auf ein Signal angewendet wird, verändert sich die enthaltene Leistung.
Die aus der Fourier-Transformation ermittelten Amplituden entsprechen dann nicht dem eigentlichen Wert.
Diese Abweichung wird als coherent gain (G) bezeichnet und lässt sich gemäß Gleichung \eqref{eq:coherent gain} berechnen.
$f(n)$ ist hierbei die Fensterfunktion und $N$ die Anzahl der Datenpunkte.
Ist $G=N$, verändert sich die Leistung nicht.
Daher bietet es sich an jedes Fenster entsprechend zu skalieren.

\begin{equation} \label{eq:coherent gain}
G = {\sum_{n=0}^{N-1}{f(n)}}
\end{equation}

Im Frequenzbereich bilden Fenster Keulen.
Eine flache Hauptkeule, wie sie das Flattop-Fenster (blau) in Abbildung \ref{fig:Fensterfunktionen Bild} hat, eignet sich besonders gut um die Amplitude eines einzelnen Sinussignals exakt zu ermitteln.
Eine schmale Hauptkeule hingegen erleichtert die Unterscheidung von zwei Sinussignalen, die eng aneinander liegen.
Nebenkeulen sind in der Regel unerwünscht, aber nicht vermeidbar.
Sie sollen daher meist möglichst gering sein.

\begin{figure}
	\centering
	\subcaptionbox{\label{fig:Fensterfunktionen Zeit}}
	[.49\textwidth]{\includegraphics{Abbildungen/fenster_zeit.eps}}
	\subcaptionbox{\label{fig:Fensterfunktionen Bild}}
	[.49\textwidth]{\includegraphics{Abbildungen/fenster_frequenz.eps}}
	\caption[Verschiedene Fensterfunktionen]{
	\subref{fig:Fensterfunktionen Zeit} zeigt den Verlauf der Fensterfunktionen Flattop (blau), von Hann (rot) und Hamming (gelb) im Zeitbereich.
	
	\subref{fig:Fensterfunktionen Bild} zeigt den Verlauf der Fensterfunktionen im Frequenzbereich mit der Länge $N=32$.
	}
	\label{fig:Fensterfunktionen}
\end{figure}

In Abbildung \ref{fig:Fenster_Fehler} wird gezeigt, welchen Einfluss die Fensterwahl auf die Bestimmung des Effektivwertes hat.
Es wurde Sinussignal mit einer Länge $N=2048$ erzeugt.
Die Zahl der Perioden des Signals lag zwischen vier und sechs.
Anschließend wurde das Signal gefenstert und der coherent gain ausgeglichen.
Im Vergleich zu anderen Fenstern ist durch das Falttop-Fenster die Bestimmung der Hauptamplitude am besten möglich.
Der durch Leakage verursachte Fehler liegt bei 1,11\textperthousand. Die Anzahl der Perioden hat keinen Einfluss auf den durch den Leakage-Effekt verursachten Fehler.

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{Abbildungen/fenster_vergleich.eps}
	\caption[Fehler verschiedener Fensterfunktionen]{
	Relativer Fehler verschiedener Fenster. Blackmann-Harris (blau), Flattop (rot), Hamming (gelb), von Hann (violett) und Rechteck (grün).
	
	Für diesen Vergleich wurden Sinussignale unterschiedlicher Frequenz erzeugt, so dass sich bei einer Datenlänge von $N=2048$ vier bis sechs Perioden ergeben.
	Anschließend wurden die Signale mit oben genannten Fenstern multipliziert und der coherent gain ausgeglichen.
	Beim dem Rechteck-Fenster treten die größten Leakage-Effekte auf. Der Leakagefehler des Flattop-Fensters ist um ca. zwei Größenordnungen geringer als bei allen anderen untersuchten Fenster.
	}
	\label{fig:Fenster_Fehler}
\end{figure}
%\section{Autokorrelation}
%\label{sec:Autokorrelation}
%Periodische Signale zeichnen sich durch ihre stationären Parameter aus.
%Sinussignale lassen sich über ihre Amplitude, die Frequenz und die Phase für jeden beliebigen Zeitpunkt bestimmen.
%Ein Sinussignal ist somit deterministisch.
%
%Bei Messungen an realen Systemen werden die idealen, periodischen Signale jedoch von stochastischen Signalen überlagert.
%Die innerhalb der Arbeit gemessenen Sinussignale sind durch externe Störeinfüsse, aber auch durch die Messschaltung selbst (u.a. nicht lineare Verstärkung, thermisches Rauschen) nicht exakt vorhersagbar.
%Schneidet man zwei Ausschnitte des selben Signals mit selber Amplitude, Frequenz und Phasenlage heraus, werden diese Signale nicht identisch sein, aber ähnlich.
%Diese Ähnlichkeit lässt sich nicht nur subjektiv feststellen, sondern auch objektiv berechnen.
%Dazu wird die Korrelationsfunktion gemäß Gleichung \eqref{eq:Korrelationsfunktion} benutzt.
%\begin{equation} \label{eq:Korrelationsfunktion}
%r_{xy}(\tau) = \int_{-\infty}^{\infty}{x(t)y(t+\tau)dt}
%\end{equation}
%Die Funktion $r_{xy}(\tau)$ beschriebt dabei die Ähnlichkeit des Signals $y$ zu dem Signal $x$, wenn dieses um die Zeit $\tau$ verschoben ist.
%Natürlich kann die Korrelationsfunktion nicht nur auf zwei unabhängige Funktionen angewendet werden, sondern auch ein Signal mit sich selbst korreliert werden.
%Die draus resultierende Funktion $r_{xx}(\tau)$ wird Autokorrelationsfunktion genannt.
%Die Autokorrelationsfunktion lässt sich nicht nur für zeitkontinuierliche, sondern nach Gleichung \eqref{eq:DAKF} auch für zeitdiskrete Werte bestimmen.
%Dazu wird die Integration durch eine Summe ersetzt.
%\begin{equation} \label{eq:DAKF}
%r_{xx}(k) = \sum_{-\infty}^{\infty}{x(n)x(n+k)dt}
%\end{equation}
%
%Die Autokorrelation wird in \cite{eickmann2015} genutzt um die Periodendauer eines verrauschten Signales exakter zu bestimmen.
%Abbildung \ref{fig:Sinus_verrauscht} zeigt ein Sinussignal mit einer Amplitude von 1 und einer Frequenz von \SI{100}{\hertz}, das von einem nicht deterministischen, weißen Rauschen überlagert ist.
%Das Signal wird mit einer Frequenz von \SI{1}{\kilo\hertz} abgetastet.
%In Abbildung \ref{fig:AKF_sinus} ist die zugehörige Autokorrelationsfunktion des Signals dargestellt.
%Bei dem ursprünglichen Signal verschieben sich die Nulldurchgänge, aber auch die Extremwerte des Sinussignals.
%Eine eindeutige Bestimmung der Periodendauer ist nicht mehr möglich.
%Das autokorrlierte Signal hingegen ist rauschfrei.
%Dadurch lassen sich die Perioden und auch die Periodendauer einfacher ermitteln. 
%\begin{figure}
	%\centering
	%\subcaptionbox{\label{fig:Sinus_verrauscht}}
	%[.49\textwidth]{\includegraphics{Abbildungen/akf_rohsignal.eps}}
	%\subcaptionbox{\label{fig:AKF_sinus}}
	%[.49\textwidth]{\includegraphics{Abbildungen/akf_akf.eps}}
	%\caption[Autokorrelation]{
	%Bei Abbildung \subref{fig:Sinus_verrauscht} handelt es sich um ein verrauschtes Sinussignal. Periodendauer und Amplitude lassen sich nur schwer ermitteln.
	%
	%Abbildung \subref{fig:AKF_sinus} ist das autokorrelierte Signal des Sinus.
	%Dieses Signal ist rauschfrei und die Periodendauer lässt sich leicht ermitteln.
	%}
	%\label{fig:Autokorrelation}
%\end{figure}
%
%Vergleicht man die Faltungsfunktion gemäß Gleichung \eqref{eq:Faltung} mit der Korrelationsfunktion aus Gleichung \eqref{eq:Korrelationsfunktion} fallen Ähnlichkeiten auf.
%Die beiden Funktionen unterscheiden sich durch ihre Integrationsvariable und das Vorzeichen der Verschiebung.
%\begin{align} \label{eq:Faltung}
%y(t) = \int_{-\infty}^{\infty}{x(\tau)h(t-\tau)d\tau} \\
%y(t)= x(t)\ast h(t) \nonumber
%\end{align}
%Somit lässt sich eine Korrelation auch als Faltung gemäß Gleichung \eqref{eq:Korrelation als Faltung} schreiben. Dabei wird das Signal $x(t)$ gespiegelt und mit dem ungespiegelten Signal $y(t)$ gefaltet. Anschließend erfolgt eine Substitution von $t$ durch $\tau$. \cite[56]{Gruenigen2014}
%\begin{equation} \label{eq:Korrelation als Faltung}
%r_{xy}(\tau) = x(-t)\ast y(t)|_{t*\tau}
%\end{equation}
%Aus Abschnitt \ref{sec:Fourier-Transformation} ist bereits der Faltungssatz aus Gleichung \eqref{eq:FT Faltungssatz} bekannt.
%Eine Faltung im Zeitbereich entspricht einer Multiplikation im Frequenzbereich.
%Diese Eigenschaft lässt sich nutzen um die Korrelation nach Gleichung \eqref{eq:Korrelationstheorem} zu schreiben.
%\begin{equation} \label{eq:Korrelationstheorem}
%r_{xy}(\tau) = \laplace X^*(f) \cdot Y(f)
%\end{equation}
%$X^*(f)$ist hierbei das konjungierte Signal der fourier-transformierten von $x(t)$ und $Y(f)$ das fourier-transformierte Signal von $y(t)$.
%
%Dieser Zusammenhang lässt sich nutzen, um den Berechnungsaufwand zusammen mit der \ac{FFT} zu minimieren.
%Eine diskrete Korrelation nach Gleichung \eqref{eq:DAKF} benötigt für jeden einzelnen Punkt $N$ Berechnungen.
%Der Rechenaufwand steigt somit quadratisch an.
%Bei Verwendung des Korrelationstheorems muss sowohl $x(t)$ als auch $y(t)$ fourier-transformiert werden, sowie später eine Rücktransformation stattfinden.
%Dies benötigt jeweils $N \log_2(N)$ Berechnungen.
%Die Konjungierung und die Multiplikation benötigen beide jeweils $N$ Berechnungen. Für große $N$ ergeben sich hier dennoch massive Zeiteinsparungen, ähnlich wie bei der schnellen Fourier-Transformation in Abschnitt \ref{sec:FFT}.
