\chapter{Validierung der Messkarte und Parameterermittlung}
\label{sec:Validierung}
Nach der Entwicklung der Messkarte in Kapitel \ref{sec:Messkarte} und der Implementierung der Signalverarbeitung in Kapitel \ref{sec:Implementierung HW} und \ref{sec:Implementierung SW} erfolgen nun Messungen um die Messkarte zu validieren.
In Abschnitt \ref{sec:Val_Sensor} wird der sensierende Bereich der Messkarte überprüft.
Dazu gehören der einstellbare Verstärker aus Abschnitt \ref{sec:Verstärker} und die Elektroden.
Bei der Entwicklung der Messkarte wurden, wie in Abschnitt \ref{sec:Guarding} gezeigt, mehrere Möglichkeiten zum Schutz vor störenden Einflüssen in Betracht gezogen.
Diese werden in Abschnitt \ref{sec:Val_S_CT} miteinander verglichen.
Anschließend wird der emittierende Bereich der Messkarte in Abschnitt \ref{sec:Val_Emitter} überprüft.
Der zugehörige Verstärker ist nicht einstellbar.
Im Gegensatz zur Sensierung ist bei den Analogmultiplexern der Durchgangswiderstand von Bedeutung und wird messtechnisch ermittelt.

Abschnitt \ref{sec:Val_AD} ermittelt, wie genau der AD-Wandler Signale misst.
Außerdem wird der Einfluss des Downsamplings aus Abschnitt \ref{sec:Downsampling} untersucht.

Ein Problem bei vorherigen Arbeiten wie \cite{eickmann2015} war, dass die Signalverarbeitung lange Laufzeiten hatte.
In Abschnitt \ref{sec:Berechnungszeit} werden daher die Laufzeiten der einzelnen Signalverarbeitungsschritte gemessen.

\section{Sensierung}
\label{sec:Val_Sensor}
Innerhalb dieses Abschnittes wird die Messkarte von den Elektroden $S_1$ bis $S_{16}$ bis zum differenziellen Messpunkt $MP_A$ untersucht.
Zwischen diesen beiden Punkten befinden sich die Impedanzwandler, die beiden Analogmultiplexer 1 und 2 sowie der Verstärker mit einstellbarer Verstärkung.
Die Verstärkung wird in Abschnitt \ref{sec:Val_S_V} untersucht.
Durch unterschiedliche Verstärkungen ändert sich nicht nur die Amplitude sondern auch die Phasenlage, die in Abschnitt \ref{sec:Val_S_Phase} ermittelt wird.
Der Crosstalk wird in Abschnitt \ref{sec:Val_S_CT} untersucht.
Außerdem werden die in Abschnitt \ref{sec:Guarding} entwickelten unterschiedlichen Möglichkeiten zur Verringerung des Crosstalks untersucht.

\subsection{Verstärkung}
\label{sec:Val_S_V}
In Abschnitt \ref{sec:Verstärker} wurde der Verstärker mit einstellbarer Verstärkung entwickelt.
Dieser wird daher, wie in Abbildung \ref{fig:MAB V Sensor} dargestellt, geprüft.
Die beiden Frequenzgeneratoren des Oszilloskops G1 und G2 werden dazu mit den Anschlüssen für die sensierenden Elektroden $S_1$ und $S_2$ verbunden.
CH1 und CH2 des Oszilloskops werden ebenfalls mit dem Frequenzgeneratoren verbunden.
CH3 wird mit dem positiven Messpunkt des differenziellen Messpunktes A verbunden, CH4 mit dem negativen.
Die Verstärkung ergibt sich somit gemäß Gleichung \eqref{eq:V_S}.
Über den \ac{FMC}-Stecker wird der Multiplexer 1 auf den Anschluss $S_1$ gestellt, sowie der Multiplexer 2 auf $S_2$.
die Verstärkung des Verstärkers wird ebenfalls über den \ac{FMC}-Stecker vorgegeben.

\begin{equation} \label{eq:V_S}
V=\frac{U_{CH1}-U_{CH2}}{U_{CH3}-U_{CH4}}
\end{equation}

\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:MAB V Sensor}}
	[.49\textwidth]{\input{BSB_S_V}}
	\subcaptionbox{\label{fig:MAB CT Sensor}}
	[.49\textwidth]{\input{BSB_S_CT}}
	
	\caption[Messaufbauten zur Überprüfung der Sensierung]{
	\subref{fig:MAB V Sensor} zeigt den Messaufbau zur Messung der Verstärkung.
	Über den \ac{FMC}-Stecker werden der Multiplexer 1 auf den Anschluss $S_1$ und Multiplexer 2 auf $S_2$ gestellt.
	Die Verstärkung wird für die Messung ebenfalls über den \ac{FMC}-Stecker eingestellt.
	Die Anschlüsse G1 und G2 des Oszilloskops werden mit den Anschlüssen $S_1$ und $S_2$ verbunden.
	Mit CH1 wird das Signal an $S_1$ und mit CH2 an $S_2$ gemessen.
	CH3 und Ch4 dienen dazu, das differenzielle Signal am Messpunkt $MP_A$ zu messen.
	
	\subref{fig:MAB CT Sensor} zeigt den Messaufbau zur Messung des Crosstalks.
	Über den FMC-Stecker werden die Mutlplexer 1 auf $S_1$ und 2 auf $S_{16}$ gestellt.
	Die Verstärkung wird auf 12 gestellt.
	Der Frequenzgenerator G1 wird mit CH1 und $S_1$ verbunden und ein Störsignal ausgegeben.
	Der Frequenzgenerator G1 wird mit CH16 und $S_{16}$ verbunden, mit im wird eine Spannung von \SI{0}{\volt} ausgegeben.
	CH2 misst den Crosstalk an den Anschlüssen $S_2$ bis $S_{15}$.
	Mit CH3 und CH4 wird die Spannung am differenzellen Messpunkt $MP_A$ gemessen.	
	}
	\label{fig:MAB S}
\end{figure}

Die erwartet größte Verstärkung nach Tabelle \ref{tab:Widerstandswerte Verstärkung} liegt bei 1000 und die minimal ausgebbare Amplitude des Frequenzgenerators bei \SI{10}{\milli\volt}.
Daraus ergibt sich eine Spannung von \SI{10}{\volt} am Ausgang des Verstärkers.
Da Betriebsspannung jedoch nur bei \SI{3.3}{\volt} liegt, muss ein Trick angewendet werden.
Anstatt den Frequenzgenerator 2 konstant eine Spannung von \SI{0}{\volt} ausgeben zu lassen, gibt er ebenfalls ein Sinussignal aus.
Die beiden Signale werden vom Verstärker subtrahiert, so dass sich das resultierende Signal gemäß Gleichung \eqref{eq:Subtraktion Verstärkung} ergibt.
$A$ ist dabei die eingestellte Amplitude der Frequenzgeneratoren und $\phi$ die Phasenverschiebung.
Da die Angabe der Phasenverschiebungen nur ganzzahlig in Grad erfolgen kann, ist theoretisch eine minimale Amplitude von \SI{174.53}{\micro\volt} möglich.

\begin{align} \label{eq:Subtraktion Verstärkung}
U_{in}(t) &= A\sin\left(2\pi ft\right)-A\sin\left(2\pi ft+\phi \right) \\
&=-2A\sin\left(\frac{\phi }{2}\right)\cdot \cos\left(2\pi ft+\frac{\phi }{2}\right) \nonumber
\end{align}

Der Phasenverschiebungswinkel $\phi$ für eine gewünschte Amplitude ergibt sich gemäß Gleichung \eqref{eq:phi kleine Amplituden}, $A_{min}$ ist hierbei die minimal mögliche Amplitude und $A$ die gewünschte Amplitude.

\begin{equation} \label{eq:phi kleine Amplituden}
\phi\left(A_{min},A\right) = 2\sin\left(\frac{A}{-2A_{min}}\right)
\end{equation}

Abbildung \ref{fig:V Vergleich} zeigt den Verlauf der gemessenen Verstärkung bei einem Sinussignal von \SI{100}{\hertz} (blaue Kreuze) und vergleicht ihn mit den Werten der erwarteten Verstärkung aus Tabelle \ref{tab:Widerstandswerte Verstärkung} (rote Kreise).
Dabei scheinen die gemessenen Werte für die Stufen 1 bis 12 gut mit den berechneten Werten übereinzustimmen.
Bei Stufe 7 ist mit einer Verstärkung von ca. 1 eine Fehlfunktion vorhanden.
In Abbildung \ref{fig:V Vergleich Fehler} ist der relative Fehler der Verstärkung dargestellt.
Dort zeigt sich auch eine zunehmende Abweichung bei den Stufen 13 bis 16.

\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:V Vergleich}}
	[.49\textwidth]{\includegraphics{Abbildungen/V_Vergleich.eps}}
	\subcaptionbox{\label{fig:V Vergleich Fehler}}
	[.49\textwidth]{\includegraphics{Abbildungen/V_Vergleich_Fehler.eps}}
	\caption[Vergleich der berechneten und gemessenen Verstärkung]
	{\subref{fig:V Vergleich} zeigt die gemessene Verstärkung von \SI{100}{\hertz} Sinussignal (blaue Kreuze).
	Ebenfalls ist auch die erwartete Verstärkung gemäß Tabelle \ref{tab:Widerstandswerte Verstärkung} (rote Kreise).
	
	\subref{fig:V Vergleich Fehler} zeigt den relativen Fehler der Verstärkung.
	Mit Ausnahme des Ausreißers der Stufe 7 ist die Abweichung bis zur Stufe 12 gering.
	Bei den Stufen 13 bis 16 stellt sich eine zunehmend größer werdende Abweichung ein.
	}
	\label{fig:Verstärkungsvergleich}
\end{figure}

Bei der Überprüfung der Verstärkung fiel eine Frequenzabhängigkeit auf.
In Abbildung \ref{fig:Verstärkung Frequenzgang} wurde der Frequenzgang aller Verstärkungsstufen in einem Frequenzbereich von \SI{100}{\hertz} bis \SI{1}{\mega\hertz} gemessen.
Dabei fällt auf, dass die Verstärkung der Stufen 1 bis 12 bis zu einer Frequenz von ca. \SI{600}{\kilo\hertz} zunimmt.
Die Verstärkung der Stufen 13 bis 16 hingegen bleibt bis ca. \SI{100}{\kilo\hertz} konstant bevor sie abnimmt.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{Abbildungen/V_Vergleich_frequenzgang.eps}
	\caption[Frequenzabhängigkeit der gemessenen Verstärkung]
	{
	Die Verstärkung ist frequenzabhängig.
	Mit steigender Frequenz zeigt sich eine zunehmend größer werdende Verstärkung bei den Stufen 1 bis 12.
	Stufe 7 zeigt auch hier eine deutliche Abweichung von der vorgesehenen Verstärkung.	
	}
	\label{fig:Verstärkung Frequenzgang}
\end{figure}

Um das Verhalten weiter zu analysieren, wurde der Messaufbau aus Abbildung \ref{fig:MAB V Sensor} angepasst.
$S_1$ und $S_2$ wurden miteinander verbunden und die Verstärkung auf Stufe 5 gestellt,
Das Kurzgeschlossene Signal wird damit um den Faktor 8,51 verstärkt.
Das Ausgangssignal ist in Abbildung \ref{fig:Verstärkung Kurzschluss} dargestellt.
Ab einer Frequenz von \SI{100}{\kilo\hertz} steigen die Amplituden des Ausgangssignals an und erreichen ihr Maximum bei ca. \SI{850}{\kilo\hertz} bevor sie wieder absinken.
Der Bereich beinhaltet außerdem zahlreiche zusätzliche Spitzen.
Die größte liegt bei knapp über \SI{1}{\mega\hertz} mit einer Amplitude von \SI{456.2}{\milli\volt}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\textwidth]{Abbildungen/V_Kurzschluss.eps}
	\caption[Ergebnisse des Kurzschlussversuchs]
	{Die Eingänge $S_1$ und $S_2$ wurden kurzgeschlossen und die Spannung am Messpunkt $MP_A$ gemessen.
	Die Verstärkung wurde dabei auf Stufe 5 (8,51-fache Verstärkung) gestellt.
	Die Amplituden des Ausgangssignals steigen innerhalb einer Frequenz von \SI{100}{\kilo\hertz} bis ca. \SI{850}{\kilo\hertz} an und sinken anschließend wieder ab.
	Zusätzlich zeigen sich Peaks bei \SI{20}{\kilo\hertz}
	Der höchste liegt bei \SI{1.021}{\mega\hertz} mit einer Amplitude von \SI{456.2}{\milli\volt}.
	}
	\label{fig:Verstärkung Kurzschluss}
\end{figure}
\subsection{Phasenverschiebung}
\label{sec:Val_S_Phase}
Neben der Verstärkung ist auch die Phasenverschiebung des Verstärkers relevant.
Die Phasenverschiebung $\Delta\phi$ gibt an, wie weit zwei Sinussschwingungen voneinander verschoben sind.
Gleichung \eqref{eq:Phasenverschiebung berechnen} zeigt die Berechnung. $\Delta t$ ist hierbei die zeitliche Verschiebung, $T$ die Periodendauer der Sinussignale.

\begin{equation} \label{eq:Phasenverschiebung berechnen}
\Delta\phi = \frac{\Delta t}{T}*360^\circ
\end{equation}

Zur Ermittlung kann der bereits bekannte Messaufbau aus Abbildung \ref{fig:MAB V Sensor} verwendet werden.
Die Differenz zwischen CH1 und CH2 bildet das Eingangssignal und die Differenz zwischen CH3 und CH4 das Ausgangssignal. 
$T$ ist durch die Vorgabe der Frequenz bekannt.
$\Delta t$ lässt sich durch die Kreuzkorrelation gemäß Gleichung \eqref{eq:Korrelationsfunktion} bestimmen.
Liest man die Abszisse zum Zeitpunkt des Maximalwerts der Kreuzkorrelation ab, erhält man $\Delta t$.
\begin{equation} \label{eq:Korrelationsfunktion}
r_{xy}(\tau) = \int_{-\infty}^{\infty}{x(t)y(t+\tau)dt}
\end{equation}

In Abbildung \ref{fig:S_V_phi} ist die Phasenverschiebung $\Delta \phi$ in Abhängigkeit der Verstärkungsstufe und Frequenz dargestellt.
Auffallend ist vor allem die große Phasenverschiebung von bis zu $70^\circ$ bei hohen Frequenzen und geringer Verstärkungsstufe.
Auch zeigt sich wieder, dass die Stufe 7 ein anderes Verhalten zeigt.
Bei den Verstärkungsstufen 13 bis 16 zeigt sich bei hohen Frequenzen ein Sprung der Phasenverschiebung.
Dies kann jedoch auch daran liegen, dass die mit dem Oszilloskop gemessenen Eingangssignale bei hohen Verstärkungsstufen nur eine geringe Amplitude hatten, die von allgemeinem Rauschen überlagert war.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\textwidth]{Abbildungen/V_phi.eps}
	\caption[Phasenverschiebung des Sensors]
	{Die Phasenverschiebung $\Delta \phi$ des Sensors in Abhängikeit der Verstärkungsstufe und der Frequenz.
	Bei geringen Verstärkungstufen verändert sich der Phasenverschiebungswinkel von nahe $0^\circ$ bei niedrigen Frequenzen auf $70^\circ$ bei hohen Frequenzen.
	Bei hohen Verstärkungen kommt es bei hochen Frequenzen zu einem Sprung der Phasemverschiebung.
	Hier kännte die geringe Amplitude des Eingangssignals und die damit verbundene Messungenauigkeit als Fehlerursache nicht ausgeschlossen werden.
	}
	\label{fig:S_V_phi}
\end{figure}

\subsection{Crosstalk}
\label{sec:Val_S_CT}

Der Crosstalk gibt an, wie stark benachbarte Signale das eigentliche Signal beeinflussen.
Die Berechnung ergibt sich aus Gleichung \eqref{eq:Crosstalk}.
Der Messaufbau ist in Abbildung \ref{fig:MAB CT Sensor} dargestellt.
Das Störsignal wird mit dem Frequenzgenerator G1 des Oszilloskops erzeugt und mit dem Anschluss $S_1$ verbunden.
Der Frequenzgenerator G2 gibt nur ein Gleichspannungssignal von \SI{0}{\volt} aus und ist mit $S_{16}$ verbunden.
Das Störsignal wird mit CH1 gemessen.
CH2 misst das gestörte Signal an den Anschlüssen $S_2$ bis $S_{16}$.
CH3 und CH4 messen die Signale am differenziellen Messpunkt $MP_A$.
Während Multiplexer 2 dauerhaft auf $S_{16}$ geschaltet ist, wird Multiplexer 1 entsprechend auf die Anschlüsse $S_2$ bis $S_{15}$ geschaltet.
Der Verstärker wird auf die Stufe 12 geschaltet.
Dies bedeutet eine Verstärkung von ca. 180.
Aus Abbildung \ref{fig:Verstärkung Frequenzgang} ist ersichtlich, dass diese Stufe einen relativ konstanten Frequenzgang hat.

Bei den Messungen der Elektroden $S_2$ bis $S_{15}$ konnten keine wesentlichen Unterschiede festgestellt werden.
Allerdings zeigte sich eine Frequenzabhängigkeit.
Für eine genauere Ermittlung der Frequenzabhängigkeit wurde über alle Elektroden der Mittelwert gebildet und in Abbildung \ref{fig:Nebensprechen_Sensor_Array} aufgezeichnet.
Zusätzlich ist die Standardabweichung eingezeichnet.

\begin{figure}[ht!]
	\centering
		\centering
	\subcaptionbox{\label{fig:Nebensprechen_Sensor_Array}}
	[.49\textwidth]{\includegraphics[width=.49\textwidth]{Abbildungen/Nebensprechen_Sensor_Array.eps}}
	\subcaptionbox{\label{fig:Crosstalk Sensor Varianten}}
	[.49\textwidth]{\includegraphics[width=.49\textwidth]{Abbildungen/Nebensprechen_Sensor_Varianten.eps}}
	
	\caption[Messung des Crosstalks am sensierendem Elektrodenarray]{\subref{fig:Nebensprechen_Sensor_Array} zeigt die den frequenzabhängigen Crosstalk der sensierenden Elektrodenanschlüsse $S_2$ bis $S_{15}$.
	Da sich die einzelnen Anschlüsse kaum unterschieden, wurde der Mittelwert gebildet und die Standardabweichung eingezeichnet.
	
	\subref{fig:Crosstalk Sensor Varianten} zeigt den frequenzabängigen Crosstalk bei unterschiedlichen Beschaltungen der Guarding-Flächen.
	In blau ist der Verlauf des Crosstalks aus der vorherigen Messung an der Elektrode $S_2$ zu sehen.
	Die Messung in rot wurde mit zusätzlicher Verbindung nach Masse durchgeführt.
	Die Messung in gelb zeigt die Werte, wenn die Guarding-Fläche an das rückgekoppelte Signal angeschlossen ist.
	Dazu wurde die Schirmung der Verbindung zum Oszilloskop aufgetrennt.
	Ebenfalls bei aufgetrennter Schirmung wurde die in violett eingezeichnete Messung durchgeführt.
	Hier ist die Guarding-Fläche mit keinen Anschluss versehen.

	}
	\label{fig:Messung CT Sensor}
\end{figure}

In Abschnitt \ref{sec:Guarding} wurde eine Guarding-Fläche thematisiert, die die Leiterbahnen zwischen Elektrodenanschluss und Impedanzwandler umschließt.
Sie bietet verschiedene Möglichkeiten den Crosstalk zu verringern.
Bei den vorherigen Messungen war die Fläche nicht angeschlossen. Sie wurde jedoch durch den Anschluss des Schirmes an das Oszilloskops mit Masse verbunden.

Mit dem bereits bekannten Messaufbau aus Abbildung \ref{fig:MAB CT Sensor} wurde auch der Einfluss der Flächen untersucht.
Da die vorherigen Messungen keinen Unterschied bezüglich der ausgewählten Elektrode zeigten, wurde das Verhalten nur an der Elektrode $S_2$ untersucht.

Abbilung \ref{fig:Crosstalk Sensor Varianten} zeigt das Ergebnis der Messung.
Die Messungen mit einer Masse als Schirmung zeigten den geringsten Crosstalk.
Wenn die Fläche sowohl am Oszilloskop als auch auf der Messkarte mit Masse verbunden ist, bleibt der Crosstalk im gesamten Frequenzbereich unterhalb -62,2dB.
Ist die Fläche nur über das Oszilloskop mit Masse verbunden, steigt der Crosstalk mit der Frequenz an.
Das Maximum von -51,54 dB erreicht er bei \SI{1}{\mega\hertz}.

Die Messungen mit rückgekoppelter Fläche zeigten ein schlechteres Verhalten.
Bereits bei einer Frequenz von \SI{100}{\hertz} liegt der Crosstalk bei -52,25 dB.
Ab einer Frequenz von \SI{1.5}{\kilo\hertz} steigt der Crosstalk bis zu einer Frequenz von \SI{150}{\kilo\hertz} stetig an bis auf -25 dB.
Ab einer Frequenz von \SI{631}{\kilo\hertz} verhält sich dieser Aufbau nahezu identisch zur rückgekoppelten Fläche.

Die Messung mit der Fläche ohne irgendeinen Anschluss zeigte das schlechteste Verhalten. Der beste Wert lag bei -47,31 dB bei einer Frequenz von \SI{100}{\hertz}.
Bei einer Frequenz von \SI{1.5}{\kilo\hertz} betrug er bereits -37dB.

\section{Emittierung}
\label{sec:Val_Emitter}
Innerhalb dieses Abschnittes wird die Messkarte von den Eingängen des Signalerzeugers $SIG_1$ und $SIG_2$ zum differenziellen Messpunkt $MP_E$ als auch bis zu den Elektroden $E_1$ bis $E_{16}$ untersucht.
Zum Messpunkt $MP_E$ wird das Signal dabei von einem nicht einstellbaren Verstärker verstärkt.
Zwischen Eingang und Elektroden befinden sich die Multiplexer 3 und 4.
Die Verstärkung wird in Abschnitt \ref{sec:Val_E_V} gemessen, die Phasenverschiebung in Abschnitt \ref{sec:Val_E_Phase}.
Der Crosstalk wird in Abschnitt \ref{sec:Val_E_CT} untersucht.
Der Durchgangswiderstand vom Eingang bis zum Ausgang wird in Abschnitt \ref{sec:Val_E_RON} ermittelt.

\subsection{Verstärkung}
\label{sec:Val_E_V}
Abbildung \ref{fig:MAB V Emitter} zeigt den Messaufbau, der verwendet wurde um den Verstärker zwischen den Anschlüssen $SIG_1$ und $SIG_2$ und dem Messpunkt $MP_B$ bzw. dem AD-Wandler zu messen.
Der Frequenzgenerator G1 wurde dazu sowohl mit CH1 als auch mit $SIG_1$ verbunden, der Frequenzgenerator entsprechend mit CH2 und $SIG_2$.
Mit CH3 und CH4 wird der differenzielle Messpunkt $MP_B$ gemessen.
Die Verstärkung des Eingangssignals lässt sich dabei wie bereits in Abschnitt \ref{sec:Val_S_V} nach Gleichung \eqref{eq:V_S} messen.

\begin{figure}[ht!]
	\centering
	\subcaptionbox{\label{fig:MAB V Emitter}}
	[.49\textwidth]{\input{BSB_E_V}}
	\subcaptionbox{\label{fig:MAB CT Emitter}}
	[.49\textwidth]{\input{BSB_E_CT}}
	
	\caption[Messaufbauten zur Überprüfung der Emitterung]{
	\subref{fig:MAB V Emitter} zeigt den Messaufbau zur Bestimmung der Verstärkung zwischen den beiden Anschlüssen $SIG_1$ und $SIG_2$ sowie dem differnziellen Messppunkt $MP_B$.
	An den Frequenzgenerator G1 werden sowohl CH1 als auch $SIG_1$ angeschlossen.
	Der Frequenzgenerator G2 wird an CH2 und $SIG_2$ angeschlossen.
	Mit CH3 und CH4 wird die Spannung am differenziellen Messpunkt $MP_B$ gemessen.
	
	\subref{fig:MAB CT Emitter} zeigt den Messaufbau für die Bestimmung des Crosstalks des Emitters.
	Über den FMC-Stecker werden die Multiplexer 3 auf $E_1$ und der Multiplexer 4 auf $E_{16}$ gestellt.
	Frequenzgenerator G1 wird an $SIG_1$ und CH1,	Frequenzgenerator G2 an $SIG_2$ angeschlossen.
	Mit CH2 wird die Spannung an den Eletrodenanschlüssen $E_2$ bis $E_{15}$ gemessen.
	}
	\label{fig:MAB E}
\end{figure}
Wie auch schon in Abschnitt \ref{sec:Val_S_V} zeigt der Verstärker in Abbildung \ref{fig:V Emitter} eine Frequenzabhängigkeit.
Für Frequenzen unter \SI{100}{\kilo\hertz} ist die Verstärkung fast konstant gleich 1.
Oberhalb der Frequenz steigt die Verstärkung stetig an.
So beträgt die Verstärkung bei \SI{1}{\mega\hertz} 5,2.

\begin{figure}[ht!]
	\centering
	\subcaptionbox{\label{fig:V Emitter}}
	[.49\textwidth]{\includegraphics{Abbildungen/V_emitter.eps}}
	\subcaptionbox{\label{fig:phi Emitter}}
	[.49\textwidth]{\includegraphics{Abbildungen/V_E_phi.eps}}

	\caption[Messung des Emiitterverstärkers]
	{
	\subref{fig:V Emitter} Zeigt den Verlauf der Verstärkung.
	Diese ist bis zu einem Wert von unter \SI{100}{\kilo\hertz} fast konstant gleich 1.
	Oberhalt steigt die Verstärkung stetig an und erreicht zum Ende der Messung bei einer Frequenz von \SI{1}{\mega\hertz} das Maximum von 5,2.
	
	\subref{fig:phi Emitter} zeigt den Phasenverschiebungswinkel $\Delta \phi$ der Verstärkung.
	Dieser ist bis zu einer Frequenz von \SI{1}{\kilo\hertz} 1 und steigt bei höheren Frequenzen auf einen Wert von bis zu $32^\circ$ an.
}
	\label{fig:V und phi Emitter}
\end{figure}

\subsection{Phasenverschiebung}
\label{sec:Val_E_Phase}
Die Phasenverschiebung nutzt den Messaufbau aus Abbildung \ref{fig:MAB V Emitter} ebenfalls.
Die Phasenverschiebung wird zwischen dem Eingangssignal $U_E = SIG_1- SIG2$ und dem Ausgangsignal am Messpunkt B $U_A = MP_{B+}-MP_{B-}$ bestimmt.
Dazu wird die Gleichung \eqref{eq:Phasenverschiebung berechnen} genutzt.
Abbildung \ref{fig:phi Emitter} zeigt, dass der Phasenverschiebungswinkel von der Frequenz abhängig ist.
Der Winkel steigt mit zunehmender Frequenz kontinuierlich an. 

\subsection{Crosstalk}
\label{sec:Val_E_CT}
Der Messaufbau zur Bestimmung des Crosstalks ist in Abbildung \ref{fig:MAB CT Emitter} dargestellt.
Mit dem Frequenzgenerator G1 wird das Störsignal erzeugt und mit CH1, als auch mit dem Anschluss $SIG_1$ verbunden.
Der Frequenzgenerator G2 wird zur Erzeugung eines \SI{0}{\volt} Gleichspannungssignals benutzt und an den Anschluss $SIG_2$ angeschlossen.
Der Multiplexer 3 leitet das Störsignal von $SIG_1$ an den Ausgang $E_1$.
Das Signal von $SIG_2$ wird vom Multiplexer 4 an den Anschluss $E_{16}$ geschaltet.
Mit dem CH2 des Oszilloskops werden die Elektrodenanschlüsse $E_2$ bis $E_{15}$ gemessen.

\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:Crosstalk Emitter 3D}}
	[.49\textwidth]{\includegraphics{Abbildungen/Nebensprechen_Emitter_Array.eps}}
	\subcaptionbox{\label{fig:Crosstalk Emitter Frequenz}}
	[.49\textwidth]{\includegraphics{Abbildungen/Nebensprechen_Emitter_Array_frequenz.eps}}
	\caption[Crosstalk des Emitters]{
	\subref{fig:Crosstalk Emitter 3D} zeigt den Verlauf des Crosstalks sowohl in Abhängigkeit der Frequenz als auch der Elektrode.
	Mit Ausnahme der Elektrodenanschlüsse $E_5$ und $E_6$ verhalten sich alle Elektrodenanschlüsse nahezu gleich.
	Auf der Messkarte liegen die Leiterbahnen der beiden abweichenden Elektrodenanschlüsse unmittelbar neben der Leiterbahn von $E_1$.
	
	\subref{fig:Crosstalk Emitter Frequenz}	zeigt die entstehenden Mittelwerte und Standardabweichungen, wenn die Elektroden gemeinsam betrachtet werden.
	Der dabei entstehende Verlauf erinnert an einen Hochpassfilter.
	}
	\label{fig:Crosstalk Emitter}
\end{figure}

In Abbildung \ref{fig:Crosstalk Emitter 3D} ist das Ergebnis der Crosstalkmessung dargestellt.
Der Frequenzgang aller Elektroden ist ähnlich.
Deshalb wurden in Abbildung \ref{fig:Crosstalk Emitter Frequenz} die Mittelwerte und Standardabweichungen aufgezeichnet.
Der Verlauf erinnert an den eines Hochpasses mit einer Grenzfrequenz von ungefähr \SI{10}{\kilo\hertz}.
Die Anschlüsse 5 und 6 weisen eine Erhöhung von ca. 2dB auf.
Auf der Messkarte liegen die Leiterbahnen für diese Anschlüsse in unmittelbarer Nachbarschaft der Leiterbahn von $S_1$.

\subsection{Durchgangswiderstand}
\label{sec:Val_E_RON}
Der Durchgangswiderstand ist nur bei den emmitierenden Analogmultiplexern von Bedeutung.
Um ihn zu messen, wird der Messaufbau nach \ref{fig:MAB E RON} verwendet.
Der Frequenzgenerator G1 wird mit dem CH1 verbunden und auch am Anschluss $SIG_1$ angeschlossen.
Der Multiplexer 3 schaltet das Signal auf den Anschluss $E_1$.
An diesen Anschlüssen ist CH2 angeschlossen und misst die Ausgangsspannung.
Ebenfalls ist ein Widerstand angeschlossen.
Er sorgt für einen Stromfluss.
Der Widerstand ist ebenso mit dem Frequenzgenerator G2 verbunden, der eine konstante Spannung von \SI{0}{\volt} ausgibt.
Als Wert für den Widerstand wurde \SI{100}{\ohm} gewählt.
So fließt bei einem Signal mit einem Effektivwert von \SI{1}{\volt} ein Strom von \SI{10}{\milli\ampere}.
Das entspricht den Testbedingungen des Datenblatts \cite[S. 2]{ADG706}.

Abbildung \ref{fig:Messung RON E} zeigt das Ergebnis der Messung.
Oberhalb einer Frequenz von \SI{100}{\kilo\hertz} steigt der Wert stark an und erreicht bei \SI{1}{\mega\hertz} einen Wert von \SI{37}{\ohm}.
Unterhalb von \SI{100}{\kilo\hertz} ist der Widerstandswert kleiner als der im Datenblatt spezifizierte maximale Wert von \SI{4.5}{\ohm} \cite[S. 2]{ADG706}.

\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:MAB E RON}}
	[.49\textwidth]{\input{BSB_E_RON}}
		\subcaptionbox{\label{fig:Messung RON E}}
	[.49\textwidth]{\includegraphics{Abbildungen/Durchgangswiderstand_Emitter.eps}}
	
	\caption[Messaufbau zur Bestimmung von $R_{ON}$ und Messergebnisse]{
	\subref{fig:MAB E RON} zeigt die den Messaufbau zur Bestimmung des Durchgangswiderstands $R_{ON}$.
	Der Frequenzgenerator G1 wird mit CH1 und dem Anschluss $SIG_1$ verbunden.
	An den Anschluss $E_1$ wird ein \SI{100}{\ohm} Widerstand angeschlossen.
	Die andere Anschluss des Widerstandes wird an den Frequenzgenerator $G2$ angeschlossen.
	Mit CH2 wird die Spannung am Widerstand gemessen.
	
	\subref{fig:Messung RON E} zeigt den frequenzabhängigen Wert des Durchgangswiderstands $R_{ON}$.
	Unterhalb von \SI{100}{\kilo\hertz} ist der Widerstandswert kleiner als im Datenblatt angegeben.
	\SI{37}{\ohm} ist der größte Wert, diesen erreicht der Widerstand bei einer Frequenz von \SI{1}{\mega\hertz}.
	}
	\label{fig:E RON}
\end{figure}

\section{AD-Wandler und Downsampling}
\label{sec:Val_AD}
Innerhalb dieses Abschnittes werden die Eigenschaften des AD-Wandlers messtechnisch untersucht.
Bei der Implementierung wurde ein Mittelwertfilter genutzt.
Dieser beeinflusst die Messung zusätzlich.

Der Messaufbau ist mit dem Messaufbau aus Abbildung \ref{fig:MAB V Emitter} identisch.
Bei den Messungen wurde mit dem Frequenzgenerator G1 ein Signal an den Anschluss $SIG_1$, mit dem Frequenzgenerator G2 konstant \SI{0}{\volt} an den Anschluss $SIG_2$ gegeben.
Mit CH3 und CH4 wurde die Spannungsdifferenz des Messpunkts $MP_B$ gemessen.
Sie stellt das Eingangssignal für den AD-Wandler dar.
Außerdem wurden die Werte des AD-Wandlers durch das \ac{SoC} aufgenommen.

Die Abtastraten des Oszilloskops und AD-Wandlers sind unterschiedlich.
Auch die Messungen konnten nicht gleichzeitig durchgeführt werden.
Die Messdauer konnte jedoch bei beiden Messungen gleich lang gewählt werden.
Ein direkter Vergleich der Signalverläufe war dennoch nicht möglich.
Deshalb wurden der Effektivwert der Signale ermittelt, und von je 10 Messungen der Mittelwert gebildet.
Die Verstärkung ergibt sich somit gemäß Gleichung \eqref{eq:V_AD_mittel}.

\begin{equation} \label{eq:V_AD_mittel}
V = \frac{\overline{U_{adc}}}{\overline{U_{MP_B}}}
\end{equation}

Für die Messung wurde der Mittelwertfilter des Downsamplings so konfiguriert, dass er den Mittelwert aus $2^{12} = 4096$ berechnet.
Somit können Frequenzen von \SI{11.9209}{\hertz} bis \SI{12.2}{\kilo\hertz} vom AD-Wandler gemessen werden.

In Abbildung \ref{fig:Val_AD} ist das Ergebnis der Messungen in Abhängigkeit der Frequenz in blau dargestellt.
Zur Mittlung der Messungen wurden jeweils 10 Messungen durchgeführt.

Bei Frequenzen oberhalb von \SI{500}{\hertz} verringert sich die Verstärkung zunehmend. 
Diese Verringerung lässt sich auf den Mittelwertfilter zurückführen.
Dessen theoretischer Frequenzgang ist in Abbildung \ref{fig:Val_AD} in rot eingezeichnet.

\begin{figure}[ht]
	\centering
	\includegraphics{Abbildungen/Validierung_Downsampling.eps}
	\caption[Validierung des AD-Wandlers und des Downsampling]{
		Messergebnis der Verstärkung des AD-Wanders (blau).
		Die Messung jedes Messpunkts wurde 10 mal durchgeführt und der Mittelwert gebildet, ebenso die Standardabweichung.
		Bis zu einem Frequenz von ca.\SI{500}{\hertz} beträgt die Verstärkung 1.
		Darüber hinaus verringert sich die Verstärkung.
		Diese Verringerung folgt dem Verlauf der durch den Mittelwertfilter verursachten Dämpfung (rot).		
	}
	\label{fig:Val_AD}
\end{figure}

\section{Berechnungszeiten}
\label{sec:Val_Berechnungszeiten}
Zusätzlich zu den Eigenschaften der Messkarte wird auch die Berechnungszeit der einzelnen Signalverarbeitungsschritte untersucht.
Um die Berechnungszeiten zu ermitteln, wird vor der zu messenden Berechnung ein Ausgang des ARM-Prozessors eingeschaltet.
Im Anschluss der Berechnung wird dieser Ausgang erneut umgeschaltet.
Mit dem Oszilloskop wird die Dauer zwischen den Umschaltvorgängen ermittelt.

Die schnelle Fourier-Transformation benötigte zum Schreiben in den BRAM, zur eigentlichen Transformation und zum vollständigen Lesen aus dem BRAM \SI{2.0525}{\milli\second}.
Die reine Durchführung der Fourier-Transformation inklusive Konfiguration dauerte hingegen nur \SI{94.9}{\micro\second}.
Um ein Fenster auf die Messdaten zu legen werden \SI{8.851}{\micro\second} benötigt.
Um den Maximalwert einer Fourier-Transformation zu finden und den Effektivwert dieses Signals zu ermitteln, wurde eine Dauer von \SI{898.84}{\micro\second} gemessen.
Die klassische Bestimmung des Effektivwerts über alle 2048 Datenpunkte benötigte \SI{406.06}{\micro\second}.