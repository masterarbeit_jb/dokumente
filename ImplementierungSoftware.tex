\chapter{Implementierung der Software}
\label{sec:Implementierung SW}
Nach der Implementierung der Hardwarebeschreibung in Kapitel \ref{sec:Implementierung HW} wird nun die Software implementiert.
Für die Implementierung der Software auf dem ARM-Prozessor wird das Entwicklungstool Vivado SDK verwendet.
In Abschnitt \ref{sec:VivadoSDK} wird dieses Tool vorgestellt und auch, wie die Prozessoren an ihre Umgebung angepasst werden.

\begin{figure}[ht!]
\centering
	\include{PAP_Prozessor}
	\caption[Programmablaufplan des ARM-Prozessors]{Programmablaufplan des ARM-Prozessors.
	Nach dem Booten erfolgt die Initalisierung.
	Im Anschluss werden die Eingänge gelesen.
	Wenn keine Daten im UART-Buffer vorhanden sind, die Ausgänge geschrieben und das Programm beginnt erneut mit dem Einlesen der Eingänge.
	Sind Daten im UART-Buffer vorhanden, werden diese ausgelesen.
	Die darin enthaltenen Befehle werden ausgeführt.
	Anschließend Daten per UART ausgegeben.
	Abschließend erfolgt das Schreiben der Ausgänge und das Programm beginnt wieder mit dem Einlesen der Eingänge.}
	\label{fig:PAP_Prozessor}
\end{figure}

In Abbildung \ref{fig:PAP_Prozessor} ist der Programmablaufplan des Prozessors dargestellt.
Nach dem Booten des Prozessors wird die Initialisierung durchgeführt.
Während dieser werden Treiber initialisiert und Standardwerte der Ausgänge festgelegt.

Anschließend werden die Eingänge gelesen.
Über sie werden Statusinformationen vom FPGA zum Prozessor übertragen.
Näheres ist in Abschnitt \ref{sec:Impl_Kom_FPGA} beschrieben.
Daraufhin wird geprüft, ob der \ac{UART}-Buffer Daten enthält.
Ist dies nicht der Fall, folgt darauf das Schreiben der Ausgänge.
Mit diesen werden auch Information vom Prozessor zum FPGA übertragen.

Enthält der \ac{UART}-Buffer Daten, werden diese ausgelesen.
Bei den Daten handelt es sich um Befehle, die vom PC gesendet wurden.
Einige Befehle geben die Einstellung für die Messkarte vor, andere führen die Signalverarbeitung durch.
Eine Beschreibung der Kommunikation über \ac{UART} findet in Abschnitt \ref{sec:Impl_Kom_PC} statt.
Die Signalverarbeitung ist in Abschnitt \ref{sec:Imp_Signalverarbeitung} beschrieben.

Nach erfolgreicher Ausführung des Befehls werden, falls erforderlich, die angeforderten Daten über \ac{UART} ausgegeben.
Daraufhin folgt das Schreiben der FPGA-Ausgänge.
Nach dem Schreiben der Ausgänge springt das Programm wieder zum Lesen der Eingänge und der Ablauf beginnt von dort erneut.

\section{Vivado SDK}
\label{sec:VivadoSDK}
Das Vivado SDK der Firma Xilinx ist ein Entwicklungstool.
Mit ihm kann die Software für die ARM Prozessoren des SoC entwickelt werden.
Als Programmiersprache wird C eingesetzt.
Das entwickelte Programm kann compiliert und auf die Prozessoren geladen werden.
Außerdem kann der mit Vivado entwickelte Bitstream aus dem Kapitel \ref{sec:Implementierung HW} in den FPGA geladen werden.
Es ist möglich, das entwickelte Programm zu debuggen.
Dazu kann das Programm Codezeile für Codezeile ausgeführt oder über Breakpoints die Ausführung an bestimmten Stellen unterbrochen werden.
Ist die Ausführung unterbrochen, kann der Wert von Variablen überprüft und auch manipuliert werden.

Zum Betrieb des Prozessors ist es notwendig, die äußere Verschaltung zu kennen.
Diese wird durch die Hardware vorgegeben.
Das Programm Vivado aus Abschnitt \ref{sec:Vivado} kann eine Hardwarebeschreibung für das Vivado SDK exportieren.
Diese Hardwarebeschreibung enthält alle relevanten Daten zur äußeren Verschaltung des Prozessors.

Aus dieser Hardwarebeschreibung kann das Vivado SDK ein sogenanntes \ac{BSP} erstellen.
Das \ac{BSP} kann wahlweise für ein Projekt ohne Betriebssystem oder ein Projekt mit dem Betriebssystem "`FreeRTOS"' erstellt werden.
In dem \ac{BSP} sind alle Treiber enthalten, um die vorhandene externe Peripherie anzusteuern.
Innerhalb dieser Arbeit wird auf den Einsatz eines Betriebssystems verzichtet.
Basierend auf dem \ac{BSP} wird das eigentliche Projekt erzeugt, indem die nachfolgend beschriebenen Aufgaben implementiert werden.

\section{Kommunikation zwischen Prozessor und FPGA}
\label{sec:Impl_Kom_FPGA}
Zur Kommunikation mit dem FPGA werden zwei verschiedene Methoden genutzt.
Um einzelne Variablen zwischen Prozessor und FPGA auszutauschen, werden \ac{GPIO}-Blöcke genutzt, die in Abschnitt \ref{sec:Vivado} in die Hardware hinzugefügt wurden.
Für große Datensätze werden BRAMs eingesetzt.
Diese werden von der Anbindung des AD-Wandlers aus Abschnitt \ref{sec:Auslesen AD} oder der schnellen Fourier-Transformation aus Abschnitt \ref{sec:Imp_FFT} genutzt.

\subsection{AXI GPIO}
Die "`AXI GPIO"'-Blöcke sind in der Hardware implementiert und werden vom Prozessor über den \ac{AXI-Bus} angesteuert.
Die eigentliche Ansteuerung wird vom BSP übernommen.
Der dafür verwendete Treiber muss vor der ersten Verwendung initialisiert werden.

Listing \ref{lst:GPIO_INIT} zeigt die Initialisierung eines "`AXI GPIO"'-Blocks.
Dazu wird zuerst eine Variable vom Datentyp "`XGpio"' angelegt, ebenso eine Variable vom Datentyp "`XGpio\_Config"', in der die Konfiguration gespeichert wird.
Die Funktion "`XGpio\_LookupConfig"', ermittelt die Konfiguration.
Dazu muss die "`DEVICE\_ID"' angegeben werden.
Der Name der Konstante ergibt sich aus dem Block Design aus Abbildung \ref{fig:BSB_AXIBUS}.
Anschließend kann der "`AXI GPIO"'-Block mit der Funktion "`XGpio\_CfgInitialize"' initialisiert werden.
Die Funktion "`XGpio\_SetDataDirection"' dient zur Vorgabe der Datenrichtung.
Der dritte Parameter wird als Maske verwendet.
1 setzt die Datenrichtung auf Ausgang, 0 auf Eingang.

\begin{lstlisting}[language=C,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:GPIO_INIT},
caption={Initalisierung der GPIO-Blöcke},
morekeywords={XGpio_LookupConfig, XGpio_CfgInitialize, XGpio_SetDataDirection, XGpio, XGpio_Config}]
XGpio PS_PL0;
XGpio_Config *GPIO_PS_PL0_config;
GPIO_PS_PL0_config = XGpio_LookupConfig(XPAR_GPIO_PS_TO_PL0_DEVICE_ID);
XGpio_CfgInitialize(&PS_PL0, GPIO_PS_PL0_config,
	GPIO_PS_PL0_config->BaseAddress);
XGpio_SetDataDirection(&PS_PL0, CH_PS_PL_0, 0xFFFFFFFF);
XGpio_SetDataDirection(&PS_PL0, CH_PS_PL_1, 0xFFFFFFFF);	
\end{lstlisting}

Nach der Initialisierung können die "`AXI GPIO"'-Blöcke ausgelesen werden.
Zum Lesen stellt das BSP die Funktion "`XGpio\_DiscreteRead"' bereit.
Sie gibt einen 32 Bit Integer zurück, der den aktuellen Zustand des "`AXI GPIO"'-Blocks enthält.
Die Funktion "`XGpio\_DiscreteWrite"' setzt die Ausgänge des "`AXI GPIO"'-Blocks auf den im dritten Parameter enthaltenen 32 Bit Integer.

\begin{lstlisting}[language=C,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:GPIO_READ_WRITE},
caption={Lesen und Schreiben mit den GPIO-Blöcken},
morekeywords={XGpio_DiscreteRead, XGpio_DiscreteWrite}]
PL_PS_1.asInt = XGpio_DiscreteRead(&PL_PS, CH_PL_PS_1);
XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
\end{lstlisting}

Da der "`AXI GPIO"'-Block immer 32 Bit Daten verarbeiten, ist das Setzen von einzelnen Bits nur schwer nachzuvollziehen.
Um die Lesbarkeit des Programmcodes zu erhöhen wurde deshalb für jeden "`AXI GPIO"'-Block eine eigener Datentyp mit "`union"' definiert.
Ein "`union"' besteht aus mehreren Elementen, die auch aus unterschiedlichen Datentypen bestehen können.
Allerdings beginnen alle Elemente an der selben Speicheradresse.
Diese Eigenschaft wird genutzt um je ein Element vom Typ "`uint32t"' und vom Typ "`struct"' anzulegen.
Das Element vom Typ "`struct"' besteht wiederum aus weiteren Elementen. Diese werden jedoch im Speicher sequenziell abgelegt.
Die Größe der Elemente kann bitweise vorgegeben werden.

Listing \ref{lst:PS_PL_1} zeigt dies beispielhaft für den Datentyp "`PS\_PL\_1t"'.
Dieser Datentyp wird von der Variable "`PS\_PL\_1"' verwendet, die den Zustand eines GPIO-Ausgangs speichert.
Innerhalb des "`union"' befindet sich ein "`struct"' mit dem Namen "`s"' und ein Integer "`asInt"' vom Typ "`uint32\_t"'.
In"`s"' befindet sich der acht Bit große Integer "`LED"'.
Dieser steuert die acht LEDs auf dem ZedBoard.
"`FFT\_FWD"' ist nur ein Bit groß und stellt die Richtung der schnellen Fourier-Transformation ein.
In Zeile 12 ist der Zugriff auf alle 32 Bit der Variable "`PS\_PL\_1"'.
Zeile 13 greift nur auf das Bits von "`FFT\_FWD"' zu.

\begin{lstlisting}[language=C,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:PS_PL_1},
caption={Aufbau des Datentps "`PS\_PL\_1t."'
Dieser enthält ein "`union"' in dem sich ein "`struct"' und ein 32 Bit Integer befinden.
In einer Variable dieses Datentyps wird der Zustand eine "`AXI GPIO"'-Blocks gespeichert.},
morekeywords={uint32_t, PS_PL_1t}]
typedef union {
  struct {
		unsigned int LED 				: 8;	// 0-7
    unsigned int FFT_FWD		: 1;	// 8
		// ...
	} s;
	uint32_t asInt;
} PS_PL_1t;

PS_PL_1t PS_PL_1;

PS_PL_1.asInt = 0;
PS_PL_1.s.FFT_FWD = 1;
\end{lstlisting}

\subsection{AXI BRAM Controller}
\label{sec:ImpSW_BRAM}
Die BRAM-Speicher sind in der Hardware realisiert.
Zur Ansteuerung wird ein "`AXI BRAM Controller"' verwendet.
Dieser wird über den \ac{AXI-Bus} angesteuert.
Dazu stellt das BSP einen Treiber bereit, der gemäß Listing \ref{lst:BRAM_INIT} initialisiert wird.
Die Funktion "`XBram\_LookupConfig"' ermittelt die Konfiguration anhand der "`DEVICE\_ID"'.
Die Namenskonstante ist durch das Block Design aus Abbildung \ref{fig:BSB_AXIBUS} vorgegeben.
Die Funktion "`XBram\_CfgInitialize"' führt die eigentliche Initalisierung durch.

\begin{lstlisting}[language=C,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:BRAM_INIT},
caption={Initalisierung des BRAM-Controllers},
morekeywords={XBram_LookupConfig, XBram_CfgInitialize, XBram, XBram_Config}]
XBram BRAM_CH_A;
XBram_Config *BRAM_Config_CH_A;
BRAM_Config_CH_A = XBram_LookupConfig(XPAR_BRAM_CH_A_DEVICE_ID);
XBram_CfgInitialize(&BRAM_CH_A, BRAM_Config_CH_A, BRAM_Config_CH_A->CtrlBaseAddress);
\end{lstlisting}

Das Auslesen einer Speicheradresse erfolgt gemäß Listing \ref{lst:BRAM_READ_WRITE} mit der vom BSP vorgegebenen Funktion "`XBram\_ReadReg"'.
Mit der Funktion "`XBram\_WriteReg"' wird ein Wert in das BRAM gespeichert.
Zwar werden immer 32 Bit Integer geschrieben oder gelesen, die Adressierung über "`RegisterOffset"' erfolgt aber immer byteweise.
Die beiden Funktionen arbeiten nur mit Integer.
Allerdings werden die Werte von Seiten des FPGA bereits in 32 Bit Gleitkomma Werte in den Speicher geschrieben.
Diese Problematik wird über einen "`union"' umgangen.

\begin{lstlisting}[language=C,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:BRAM_READ_WRITE},
caption={Auslesen und Speichern mit dem BRAM-Controller},
morekeywords={XBram_ReadReg, XBram_WriteReg}]
int RegisterOffset = 0;
int value = XBram_ReadReg(BRAM_Config_CH_A->MemBaseAddress, RegisterOffset);
XBram_WriteReg(BRAM_Config_CH_A->MemBaseAddress, RegisterOffset, value);
\end{lstlisting}

\section{Signalverarbeitung}
\label{sec:Imp_Signalverarbeitung}
Bevor die Signale auf dem Prozessor weiter verarbeitet werden, müssen sie vom BRAM des FPGA eingelesen werden.
In eine Variable vom Typ "`struct"' werden Ergebnisse der Signalverarbeitung für jeden Kanal gespeichert.

In Abschnitt \ref{sec:Imp_FFT_SW} wird beschrieben wie die zuvor in Hardware implementierte Fourier-Transformation vom Prozessor konfiguriert und gestartet wird.
In Abschnitt \ref{sec:Imp_Effektivwert} wird die Berechnung des Effektivwertes erläutert.

\subsection{Schnelle Fourier-Transformation}
\label{sec:Imp_FFT_SW}

Die Fourier-Transformation ist, wie in Abschnitt \ref{sec:Imp_FFT} beschrieben, über zwei BRAM-Speicher mit dem Prozessor verbunden.
Die Speicher werden, wie in Abschnitt \ref{sec:ImpSW_BRAM} beschrieben, mit den zu transformierenden Daten gefüllt.

Anschließend erfolgt die Konfiguration.
Der IP-Core kann sowohl normale als auch inverse Fourier-Transformationen durchführen.
Außerdem muss eine Skalierung "`FFT\_SCALE"' erfolgen.
Dieser Wert ist für die innerhalb des IP-Cores vorhandenen Stufen relevant.
Jede Stufe wird innerhalb des Feldes von zwei Bit repräsentiert.
Durch diese zwei Bits wird für jede Stufe separat eingestellt, ob das Ergebnis um null bis drei Bits nach rechts verschoben werden soll.
Durch die Verschiebung wird ein Überlauf verhindert, allerdings reduziert sich die Auflösung der Transformation ebenfalls.
Wird der Wert auf 0x6AB gestellt, ist ein Überlauf bei der hier gewählten Hardwareimplementierung ausgeschlossen.\cite[S. 46]{xfft_product_guide2015}

Die Konfiguration wird anschließend per GPIO übertragen.
In einer weiteren Übertragung über die GPIO wird die Fourier-Transformation gestartet.
Anschließend wird auf die Fertigstellung der Fourier-Transformation gewartet.
Ist das über GPIO gelesene Statusbit gesetzt, werden die BRAM-Speicher ausgelesen.

Eine anschließende Division durch die Anzahl der verabeiteten Datenpunkte $N=2048$ gibt das komplexe Amplituden Spektrum wieder.
Die Frequenz der einzelnen Amplituden lässt sich gemäß Gleichung \eqref{eq:f_sfft}  bestimmen.
Sie hängt von der Abtastrate $f_s$ und der gewählten Downsampling-Stufe $ds$ ab.
\begin{equation} \label{eq:f_sfft}
\text{f}(n) = \frac{n \text{f}_s}{2^\text{ds}}
\end{equation}

\subsection{Effektivwert}
\label{sec:Imp_Effektivwert}
In \cite[S. 28]{eickmann2015} wird mit einer Autokorrelation die Periodizität des Signals ermittelt.
Anschließend werden die Messdaten so gekürzt, dass sie ein Vielfaches der Periodendauer enthalten.
Aus diesen gekürzten Messdaten wird anschließend der Effektivwert bestimmt.

In dieser Arbeit wird ein anderes Verfahren implementiert.
Die Fourier-Transformierte enthält bereits die diskreten, komplexen Amplituden des Spektrums.
Im Abschnitt \ref{sec:DFT} wurde gezeigt, wie mit der diskreten inversen Fourier-Transformation der ursprüngliche diskrete Signalverlauf im Zeitbereich ermittelt wird.
Alternativ lässt sich auch der Effektivwert einer einzelnen Signalfrequenz im Zeitbereich ermitteln.

Um spätere Ungenauigkeiten zu minimieren, wird zuerst ein Fenster mit den Messdaten multipliziert.
Das Flattop-Fenster zeigte im Abschnitt \ref{sec:Fenster} die besten Eigenschaften.
Um die Berechnungszeit gering zu halten, wurden die Werte des Fensters vorab gespeichert und im Programmcode fest hinterlegt.

Anschließend wird die Fourier-Transformation wie in \ref{sec:Imp_FFT_SW} beschrieben durchgeführt.

Für den Effektivwert ist der Betrag des komplexen Ergebnisses notwendig, daher wird dieser gebildet.
Außerdem enthält das Ergebnis auch Frequenzen oberhalb der, durch das Nyquist-Shannon-Abtasttheorem festgelegten, maximal möglichen Signalfrequenz.
Diese werden entfernt und die verbleibenden Frequenzwerte der Fourier-Transformation verdoppelt.
Eine Ausnahme stellt der Fall $f=(0)$ da, dieser Wert wird nicht verdoppelt.
Anschließend wird das Ergebnis durch $\sqrt2$ geteilt um die Effektivwerte zu erhalten.

Die Frequenz des emittierten Signals kann ermittelt werden, indem der Maximalwert innerhalb des Transformationsergebnisses gesucht wird.
Anschließend kann auch zu dieser Frequenz gehörende Effektivwert des sensierten Signals ermittelt werden.
Störungen und Rauschen können so herausgefiltert werden.

\section{Kommunikation zwischen Prozessor und PC}
\label{sec:Impl_Kom_PC}
Der PC wird über USB an das ZedBoard angeschlossem.
Auf dem ZedBoard befindet sich ein Chip, welcher eine serielle Schnittstelle emuliert.
Dieser Chip ist über \ac{UART} mit dem Prozessor verbunden.

Aus Abbildung \ref{fig:PAP_Prozessor} ist ersichtlich, dass der Prozessor regelmäßig prüft, ob Daten im Eingangsbuffer vorhanden sind.
Ist dies der Fall, werden die Daten ausgelesen.
Das erste Zeichen stellt dabei den Befehl dar.
Je nach Befehl besteht ein Befehl aus mehreren Zeichen, wie es z.B. bei der Einstellung des Multiplexers der Fall ist.
Eine Liste aller Befehle findet sich in Tabelle \ref{tab:Befehlssatz}.
Befehle, die etwas Einstellen werden um den einzustellenden Wert $NN$ ergänzt.
$NN$ ist dabei immer zweistellig und besteht aus ASCII codierten Zifferzeichen.
Die Rückgabe der Werte erfolgt binär im float Format.
So können große Datenmengen schneller übertragen werden.

\begin{table}[ht!]
\centering
		\begin{tabular}{|l|l|l|}
			 \hline
\textbf{Befehl} & \textbf{Zeichen}	& \textbf{Rückgabe}	\\ \hline
Einstellen des Verstärkers						&  cNN	& -						  \\ \hline
Einstellen des Multiplexers 1					&  dNN	& -						  \\ \hline
Einstellen des Multiplexers 2					&  eNN	& -						  \\ \hline
Einstellen des Multiplexers 3					&  fNN	& -						  \\ \hline
Einstellen des Multiplexers 4					&  gNN	& -						  \\ \hline
Einstellen des Downsamplings 					&  rNN	& -		  				\\ \hline
Neue Messung													&  n		& -		  				\\ \hline
Rohdaten des Sensors ausgeben					&  a		& float[2048]		\\ \hline
Rohdaten des Emitters ausgeben				&  b		& float[2048]		\\ \hline
Flattop-Fenster auf Rohdaten anwenden	&  q		& -		  				\\ \hline
$V_{eff-klassisch}$ Sensor ausgeben 	&  h		& float					\\ \hline
$V_{eff-klassisch}$ Emitter ausgeben	&  i		& float					\\ \hline
FFT der Sensor Werte ausgeben					&  l		& float[2048]		\\ \hline
FFT der Emitter Werte ausgeben				&  m		& float[2048]	  \\ \hline
$V_{eff}$ Sensor ausgeben (nach Abschnitt \ref{sec:Imp_Effektivwert})							&  j		& float					\\ \hline
$V_{eff}$ Emitter	ausgeben (nach Abschnitt \ref{sec:Imp_Effektivwert})								&  k		& float					\\ \hline

		\end{tabular}
		\caption[Befehlssatz]{
		Der Befehlssatz um über die UART-Schnittstelle mit dem Prozesser zu kommunizieren.
		}
		\label{tab:Befehlssatz}
\end{table}