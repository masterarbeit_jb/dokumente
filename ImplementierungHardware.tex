\chapter{Implementierung der Hardwarebeschreibung}
\label{sec:Implementierung HW}
Dieses Kapitel beschäftigt sich mit der Implementierung der Hardwarebeschreibung auf dem FPGA des SoCs auf dem ZedBoard.
In Abschnitt \ref{sec:Vivado} werden das dafür verwendete Entwicklungstool vorgestellt und die Konfiguration des SoC durchgeführt.
Die Datenverarbeitung zwischen AD-Wandler und Prozessor ist in Abschnitt \ref{sec:Auslesen AD} beschrieben.
In Abschnitt \ref{sec:Imp_FFT} wird beschrieben, wie eine schnelle Fourier-Transformation auf Hardwareebene implementiert ist.

\section{Vivado}
\label{sec:Vivado}
Um die Hardwarebeschreibung zu erstellen wird das Entwicklungstool Vivado der Firma Xilinx benutzt.
Es bietet Unterstützung für die Hardwarebeschreibungssprachen Verilog und \ac{VHDL}.
Innerhalb dieser Arbeit wird VHDL verwendet.
Vivado erstellt aus der Beschreibung eine Synthese und eine Implementierung passend für die zuvor ausgewählte Hardware.
Außerdem wird ein Bitstream erstellt, der auf das \ac{SoC} geladen werden kann.
Hardwarebeschreibungen können auch simuliert und so ihr Verhalten analysiert werden. 
Außerdem besteht die Möglichkeit, die Hardwarebeschreibung in anderen Programmen zu exportieren.
In Abschnitt \ref{sec:VivadoSDK} wird diese Möglichkeit genutzt, um die Software des ARM Prozessors mit dem FPGA zu verbinden.

Im Programmumfang ist ebenfalls eine \ac{IP-Core}-Bibliothek enthalten.
IP-Cores sind Hardwarebeschreibungen zur Lösung von bestimmten Aufgaben.
Die Hardwarebeschreibung ist jedoch codiert und nicht einsehbar.

Bei der Erstellung eines Projektes lässt sich auswählen, welche Hardware vorhanden ist.
Das ZedBoard wird als Evaluierungsboard mit aufgeführt.
Vivado erstellt daraufhin ein Block-Design, welches die Konfiguration zum Betrieb des ZedBoards enthält.


Das vollständige Block-Design ist in Abbildung \ref{fig:BSB_AXIBUS} zu sehen.
Der Block "`ZYNQ7 Processing System"' beinhaltet die Arm-Prozessoren, sie können über den Block konfiguriert werden.
Der Takt des FPGA wird auf \SI{100}{\mega\hertz} gestellt.
Der DDR-Arbeitsspeicher ist direkt mit den Prozessoren verbunden. Ebenso einige "`FIXED\_IO"'-Ports, die zwingend zum Betrieb der Prozessoren erforderlich sind.
Der Block "`Processing System Reset"' leitet einen Reset des Prozessor Systems an alle Blöcke weiter.

Weitere Blöcke sind über den \ac{AXI-Bus} mit dem Prozessor verbunden.
Der Block "`AXI Interconnect"' verbindet diese miteinander.
Für diese Arbeit werden drei \ac{GPIO}-Blöcke benötigt.
Mit ihnen werden Steuerbefehle vom Prozessor an den FPGA gegeben, sowie Statusinformationen des FPGAs abgefragt.
Ebenfalls wurden vier "`AXI BRAM-Controller"' hinzugefügt.
Zwei sind für das in Abschnitt \ref{sec:Auslesen AD} vorgestellte Auslesen des AD-Wandlers notwendig.
Die beiden anderen BRAM-Controller sind für die Anbindung an die schnelle Fourier-Transformation notwendig, die in Abschnitt \ref{sec:Imp_FFT} beschrieben ist.

\begin{figure}[ht!]
	\includegraphics[width=\textwidth]{Abbildungen/zed_design.eps}
	\caption[Vollständiges Block Design]{ Vollständiges Block-Design zur Anbindung der ARM-Prozessoren an den FPGA.
	Der Block "`ZYNQ7 Processing System"' enthält die beiden ARM-Prozessoren.
	"`Processing System Reset"' leitet den Reset des Prozessorsystems an alle Blöcke des Block Design weiter.
	Zur Verbindung weiterer Blöcke wird der \ac{AXI-Bus} eingesetzt.
	Der Block "`AXI Interconnect"' ist dabei das gemeinsame Bindeglied.
	Für die Arbeit werden drei "`AXI GPIO"' und drei "`AXI-BRAM Controller"' Blöcke benötigt.
	}
	\label{fig:BSB_AXIBUS}
\end{figure}

\section{Auslesen des AD-Wandlers}
\label{sec:Auslesen AD}

\begin{figure}[ht!]
	\include{BSB_ADinput}
	\caption[Blockschaltbild vom Auslesevorgang des AD-Wandlers]{Blockschaltbild vom Auslesevorgang des AD-Wandlers.
Die beiden Messdaten des AD-Wandler werden über 14 \ac{LVDS}-Leitungspaare von der Messkarte zum FPGA übertragen.
Der FPGA befindet sich auf einem SoC innerhalb des ZedBoards.
Auf dem FPGA werden die Daten von \ac{DDR} zu \ac{SDR} umgewandelt.
Dadurch verdoppelt sich scheinbar die Anzahl der Bits von 14 auf 28.
Da die Daten zu diesem Zeitpunkt noch synchron zum Takt des AD-Wandlers sind, werden sie in ein Dual-Port \ac{FIFO} geschrieben.
Dieses FIFO wird auf seinem zweiten Port mit dem Takt des FPGA ausgelesen.

Vor dem Downsampling erfolgt die erneute Aufteilung der beiden Messdaten.
Nach dem Downsampling werden die Daten von Integer zu Gleitkomma umgewandelt
Anschließend werden die Daten in den BRAM geschrieben.
Die Anforderung des Auslesevorgangs erfolgt mit dem Signal "`data\_request"'.
Die Beendigung wird mit dem Signal "`data\_ready"' angezeigt.
}
	\label{fig:BSB_ADinput}
\end{figure}

Abbildung \ref{fig:BSB_ADinput} zeigt, wie Daten des AD-Wandlers über das Zedboard in den FPGA gelangen
Die Daten werden in zwei seperate \ac{BRAM} gespeichert.
Von dort aus können diese vom ARM-Prozessor ausgelesen werden.
Der AD-Wandler sendet seine Daten synchron zu seinem eigenen Takt, sowohl bei steigender, als auch bei fallender Flanke, Daten (\ac{DDR}).
Die Datenleitungen des AD-Wandlers verwenden den Signalstandard \ac{LVDS}.
Für diese Aufgabe stellt Vivado einen \ac{IP-Core} bereit, der entsprechend konfiguriert und verwendet wird.
Dieser konfiguriert die Leitungen mit einem passenden Abschlusswiderstand und wandelt \ac{DDR} zu \ac{SDR} um.
In Folge dessen verdoppelt sich scheinbar die Anzahl der Datenbits.

Die Daten sind jedoch noch immer synchron zum \SI{100}{\mega\hertz} Takt des AD-Wandlers.
Im ungünstigsten Fall kann es so zu metastabilen Zuständen durch Zugriff den FPGA kommen.
Um dies zu verhindern, wird ein Dual-Port \ac{FIFO} eingesetzt.
Auf der Eingangsseite werden die Daten über die Taktleitung des AD-Wanders in das \ac{FIFO} geschrieben.
Auf der Ausgangsseite werden die Daten mit der Taktleitung des FPGA ausgelesen.
Da beide Takte gleich schnell sind und kontinuierlich geschrieben und gelesen wird, genügt ein kleiner Speicher.

Anschließend werden die Daten des sensierenden und emitierten Signals getrennt behandelt.
In Abschnitt \ref{sec:Downsampling} ist das Downsampling beschrieben.
Es wird durchgeführt um die Datenrate zu verringern.
Darauf folgt eine Umwandlung des Datentyps.
Der AD-Wandler verwendet einen Festkomma-Datentyp.
Die spätere Signalverarbeitung wird jedoch mit 32-Bit Gleitkomma durchgeführt.
Die Gleitkomma-Werte werden anschließend in einem \ac{BRAM} gespeichert.
Dieser Vorgang ist in Abschnitt \ref{sec:BRAM} beschrieben.

\subsection{Downsampling}
\label{sec:Downsampling}
Der AD-Wandler arbeitet mit einem Takt von \SI{100}{\mega\hertz}.
Somit lassen sich Frequenzen von maximal \SI{50}{\mega\hertz} erfassen, bei höheren Frequenzen kommt es gemäß dem Nyquist-Shannon-Abtasttheorem zu Alising.
Die minimal messbare Frequenz $f_{min}$ ergibt sich aus dem Kehrwert der Messdauer $T_{mess}$.
Um eine Frequenz von \SI{50}{\hertz} zu messen, müsste die Messdauer \SI{20}{\milli\second} betragen.
Innerhalb dieses Zeitraums misst der AD-Wandler 2 Millionen Werte pro Kanal und da sie als 32 Bit Gleitkommazahl gespeichert werden sollen, ergibt sich für diese Messung eine Größe von 16MB.
Der FPGA des XC7Z020 verfügt jedoch nur über 4,9 MB BRAM. \cite[S. 3]{XC7Z020}

Die Datenmenge muss reduziert werden.
Dazu wird nur jeder $N$. Wert des AD-Wandlers in den BRAM geschrieben.
Die maximal messbare Frequenz reduziert sich entsprechend um $N$.
Die im ursprünglichen Signal enthaltenen höheren Frequenzen verschwinden jedoch nicht, sondern verursachen Aliasing.

Für diese Arbeit wurde ein Mittelwertfilter implementiert, der höhere Frequenzen dämpft und so Aliasing reduziert.
Der Mittelwertfilter über $N$ Werte  entspricht einem \ac{FIR}-Filter mit $N$ Koeffizienten.
Die Koeffizienten dieses \ac{FIR} Filters sind $1/N$ groß und ergeben somit in Summe $1$.

Abbildung \ref{fig:FG_MWF} zeigt den Frequenzgang des Mittelwertfilters für die Fälle $N = \{2, 4, 32\}$. Nach der Reduzierung der Abtastfrequenz um $N$ betragen die maximalen Frequenzen $f_{max} =\{\SI{25}{\mega\hertz}, \SI{12.5}{\mega\hertz}, \SI{1.56}{\mega\hertz}\}$.
Die Dämpfung der Amplitude an diesen Frequenzen beträgt jeweils 6 dB.
\begin{figure}[ht]
	\includegraphics[width=\textwidth]{Abbildungen/Mittelwert_Frequenzgang.eps}
	\caption[Frequenzgang von Mittelwertfilter unterschiedlicher Länge]{Frequenzgang der Mittelwertfilter.
	Für den Fall $N=2$ (blau), $N=4$ (rot) und $N=32$ (gelb).
	Je höher $N$, desto niedriger ist die Grenzfrequenz der Filter.
	}
	\label{fig:FG_MWF}
\end{figure}

Das Listing \ref{lst:Downsampling} zeigt die Implementierung des Mittelwertfilters und die Reduzierung der Datenrate.
Die Reduzierung wird extern durch das Signal "`Divider"' vorgeben und gibt an um welche Zweierpotenz die Datenrate reduziert werden soll.
Die eingehenden Daten im Signal "`data\_in"' werden in dem Signal "`sample\_sum"' aufaddiert.
"`sample\_counter"' zählt die bisher aufaddierten Eingangsdaten.

Wird der Wert $2^\text{"`divider"'}-1$ erreicht, wird das Ausgangssignal "`data\_out"' berechnet.
Dazu ist eine Division durch $2^\text{"`divider"'}$ notwendig.
Diese wird durch eine Schiebeoperation nach rechts um "`divider"' Stellen realisiert.
Außerdem werden das Signal "`sample\_sum"' auf "`data\_in"' und das Signal "`downsample\_valid"' auf 1 gesetzt, um das Ausgangssignal "`data\_out"' als gültig zu markieren.

Der maximal zulässige Wert für das Signal "`divider"' ist 15.
Somit lässt sich ein Mittelwertfilter mit $N=2^{15}=32768$ realisieren.
Die Abtastrate beträgt dann \SI{3052}{\kilo\hertz}
Die maximal messbare Frequenz $f_{max}$ in dieser Einstellung wäre \SI{3}{\kilo\hertz}.
Für die eingangs betrachtete Signalfrequenz von \SI{50}{\hertz} würden nun nur noch 62 Datenpunkte benötigt.
Für beide Kanäle würden bei Verwendung der größten Stufe nur noch 496 Byte des Speichers benötigt.

\begin{lstlisting}[language=VHDL,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:Downsampling},
caption={
Hardwarebeschreibung des Mittelwertfilters}]
downsampling_p: process(CLK, data_in)
begin
	if rising_edge(CLK) then
		if sample_counter < 2**divider-1 then
			sample_counter <= sample_counter + 1;
			sample_sum <= sample_sum + signed(data_in);
			downsample_valid <= '0';
		else
			sample_counter <= (others => '0');
			downsample_valid <= '1';
			data_out <= "00" & std_logic_vector(shift_right(sample_sum,divider)(13 downto 0));
			sample_sum <= to_signed(to_integer(signed(data_in)), sample_sum'length);
		end if;
	end if;
end process downsampling_p;
\end{lstlisting}

\subsection{BRAM}
\label{sec:BRAM}
Bei dem \ac{BRAM} handelt es sich um einen Dual-Port BRAM.
Abbildung \ref{fig:BSB_BRAM} zeigt die beiden Ports A und B des BRAM.
An die Anschlüsse des Ports A werden die vom Downsampling kommenden Daten angeschlossen.
Der ARM-Prozessor wird an die Ports B angeschlossen.
Beide Ports verfügen über identische Anschlüsse und funktionieren identisch.
\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:BSB_BRAM}}
	[.49\textwidth]{\includegraphics[width=.4\textwidth]{Abbildungen/BSB_BRAM.eps}}
	\subcaptionbox{\label{fig:BSB_FFT}}
	[.49\textwidth]{\includegraphics[width=.4\textwidth]{Abbildungen/BSB_xfft.eps}}
	\caption[Blockschaltbilder BRAM und FFT]{
	\subref{fig:BSB_BRAM} zeigt das Blockschaltbild eines Dual-Port \ac{BRAM}.
	Dies verfügt über zwei identisch aufgebaute Ports, die auf den Speicher zugreifen können. \cite[S. 40]{Block_Memory_Generator_v8_3}
	
	\subref{fig:BSB_FFT} zeigt die Ports des IP-Cores zur schnellen Fourier-Transformation.
	(aus \cite[S. 7]{xfft_product_guide2015})
	}
	\label{fig:BSB_VHDL}
\end{figure}

Der Anschluss "`ENA"' aktiviert den Bereich A.
Daten, die an dem Anschluss "`DINA"' angelegt werden, werden an die Speicheradresse geschrieben, die "`ADDRA"' vorgibt.
Der Schreibvorgang wird nur durchgeführt, wenn "`WEA"' den Wert 1 hat und der Takt "`CLKA"' eine steigende Flanke aufweist.
Hat "`WEA"' den Wert 0, wird der an der Adresse gespeicherte Wert auf "`DOUTA"' ausgegeben.
"`RSTA"' sorgt für einen Reset des Speichers.
Bei "`REGCEA"' handelt es sich um einen optionalen Anschluss.
Mit ihm kann der BRAM mit einem geringeren Takt beschrieben werden.
Der Schreibvorgang erfolgt aber dennoch synchron zum Takt "`CLKA"'.

Es wurde entschieden, jeden BRAM mit nur 2048 Datenpunkten zu je 32 Bit zu belegen.
Somit verbrauchen die beiden BRAM für die Messdaten zusammen nur 32,8 KB.

Der BRAM wird über den BRAM-Controller gesteuert
Er verhindert gleichzeitige Zugriffe von dem ARM-Prozessor und dem FPGA auf den BRAM.
Außerdem übernimmt der BRAM-Controller die Adressierung des BRAM.

Das Listing \ref{lst:BRAM-Controll} zeigt den Prozess "`collect\_and\_save\_data :"' innerhalb des BRAM-Controllers.
Er enthält einen Zustandsautomaten mit den Zuständen "`IDLE"', "`COLLECT"' und "`READY"'.
"`IDLE"' ist der Initialzustand.
Er wird nur zu Beginn aufgerufen.
Wenn "`data\_request"' von außen auf 1 gesetzt wird, wechselt der Zustandsautomat in den Zustand "`COLLECT"'.
Zeigt das Signal "`data\_in\_valid"' an, dass aktuell keine gültigen Daten vorliegen, wird auch das Ausgangssignal  "`data\_out\_valid"' auf nicht gültig gesetzt.
Liegen jedoch gültige Daten vor, werden das Signal "`adr\_counter"' inkrementiert und das Signal "`data\_out\_valid"' für gültig erklärt.
Der BRAM wird nun an der Adresse "`adr\_counter"' mit dem Ausgangssignal vom Downsampling beschrieben.

Mit jedem gültigen Datenwert erhöht sich der Zählerstand von "`adr\_counter"'.
Da die Adressierung des BRAM bei 0 beginnt, ist das 2048 Werte umfassende BRAM bei Erreichen des Wertes 2047 vollständig gefüllt.
Der Zustandsautomat wechselt in den Zustand "`READY"' und zeigt den erfolgreichen Schreibprozess am Signal "`data\_ready"' an.
Wird mit dem Signal "`data\_request"' erneut ein Schreibvorgang angefordert, werden "`adr\_counter"' und das Signal "`data\_ready"' zurückgesetzt und der Zustand "`COLLECT"' erneut eingenommen.

\begin{lstlisting}[language=VHDL,numbers=left,tabsize=2,frame=single,captionpos=b,
label={lst:BRAM-Controll},
caption={
Hardwarebeschreibung des BRAM-Controller}]
collect_and_save_data:	process(CLK)
begin
	if	rising_edge(CLK)	then
		case collect_and_save_state is	
			when IDLE =>
				data_ready	<=	'0';
				if	data_request	=	'1'	then
					collect_and_save_state	<=	COLLECT;
					adr_counter	<=	to_unsigned(0,adr_counter'length);
				end	if;
			when COLLECT =>
				data_ready	<=	'0';
				if data_in_valid = '1' then
					adr_counter	<=	adr_counter	+	1;
					data_out_valid	<=	"1";
					if	adr_counter	=	2047	then
						collect_and_save_state	<=	READY;
						data_out_valid	<=	"0";
					end	if;
				else
					data_out_valid	<=	"0";
				end if;
			when READY =>
				adr_counter	<=	to_unsigned(0,adr_counter'length);
				data_ready	<=	'1';
				if	data_request	=	'1'	then
					collect_and_save_state	<=	COLLECT;
					adr_counter	<=	to_unsigned(0,adr_counter'length);
					data_out_valid	<=	"1";
					data_ready	<=	'0';
				end	if;
		end	case;
	end	if;
end	process	collect_and_save_data;
\end{lstlisting}

Um eine Frequenz in einer Fourier-Transformation bestimmen zu können, muss die Messdauer mindestens der Periodendauer der Frequenz entsprechen.
Zusammen mit dem maximalen Downsampling von $N=2^{15}=32768$ ergibt sich so eine minimal messbare Frequenz von \SI{1.5}{\hertz}.
In Abbildung \ref{fig:FB_Downsampling} sind die messbaren Frequenzen je nach Einstellung der Downsamplingstufe dargestellt.

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{Abbildungen/Downsampling_Frequenzbereiche}
	\caption[Messbare Frequenzen mit Downsampling]{
	Aus dem Zusammenspiel des Downsamplings aus Abschnitt \ref{sec:Downsampling} und der Größe des BRAMs ergeben sich die messbaren Frequenzen für jede Stufe.
	}
	\label{fig:FB_Downsampling}
\end{figure}

\section{Schnelle Fourier-Transformation}
\label{sec:Imp_FFT}
Die schnelle Fourier-Transformation wird von einem \ac{IP-Core} der Firma Xilinx durchgeführt.
Die Hardwarebeschreibung des IP-Cores wird von Xilinx nicht bekannt gegeben.
Er erlaubt die schnelle Durchführung von Fourier-Transformationen, als auch inversen Fourier-Transformationen und bietet unterschiedliche Konfigurationsmöglichkeiten bei der Implementierung.
Der IP-Core wurde für die Transformation von 2048 32-Bit Gleitkommazahlen konfiguriert.
Laut Konfigurationstool benötigt der IP-Core für die Transformation bei einem Takt von \SI{100}{\mega\hertz} \SI{94.1}{\micro\second}.
Das Konfigurationstool erstellt aus den Angaben eine VHDL-Komponente deren Ports in Abbildung \ref{fig:BSB_FFT} aufgeführt sind.

Die Ports sind durch ihre Namenspräfixe gruppiert.
Signale mit der Endung "`tdata"' enthalten Daten.
Die Endung "`tvalid"' gibt an, ob die Daten gültig sind.
"`tready"' gibt an, ob der Empfänger bereit ist, Daten aufzunehmen.
Mit "`tlast"' teilt der Sender dem Empfänger mit, dass nun die letzten Daten übertragen wurden.

Das Signal"`s\_axis\_config\_tdata"' dient zur Konfiguration zur Laufzeit.
Es ist 16 Bit groß.
Das LSB gibt an, ob eine normale oder eine inverse Fourier-Transformation durchgeführt werden soll.
Andere Bits sind bei der zuvor eingestellten Implementierung nicht relevant.

"`s\_axis\_data\_tdata"' enthält die Daten, die vom IP-Core transformiert werden sollen.
Das Ergebnis der Transformation wird im Signal "`m\_axis\_data\_tdata"' übertragen.
Alle 2048 Datenpunkte werden zeitlich hintereinander als Frame übertragen.

Die Signale "`m\_axis\_status\_tdata"' und"`m\_axis\_data\_tuser"' enthalten Statusinformationen über den Betriebszustand des IP-Cores.
Signale mit dem Präfix "`event"' geben ebenfalls Informationen zum Betrieb des IP-Cores. "`event\_frame\_started"' wird z.B. 1, wenn ein neuer Frame übertragen wird.

Für die Kommunikation zwischen ARM-Prozessor und dem IP-Core werden zwei BRAMs eingesetzt.
Eine BRAM speichert den Realanteil, der andere den Imaginäranteil der zu fourier-transformierenden Daten bzw. das Ergebnis der Fourier-Transformation.

Die Durchführung der Fourier-Transformation erfolgt dabei über einen Zustandsautomaten.
Dieser verfügt über fünf Zustände, die sequenziell durchlaufen werden: "`IDLE"', "`CONFIG"', "`RAM2FFT"', "`WAITING"', "`FFT2RAM"'.
Der Initialzustand ist "`IDLE"'.
Der Prozessor setzt per GPIO das Signal "`start"' auf 1.
Der Automat wechselt in den Zustand "`CONFIG"'.
In diesem Zustand wird geprüft, ob der IP-Core bereit ist, Konfigurationsdaten aufzunehmen.
Ist dies der Fall, werden die vom Prozessor über GPIO vorgegebenen Konfigurationsdaten in den IP-Core geladen.
Dazu wird das Signal "`s\_axis\_config\_valid"' für einen Takt auf 1 gelegt. Anschließend wird in den Zustand "`RAM2FFT"' gewechselt.

Im Zustand "`RAM2FFT"' werden die BRAM-Speicher ausgelesen.
Dazu wird zuerst die Adresse 0 ausgewählt.
Sofern der IP-Core seine Bereitschaft mit "`s\_axis\_data\_tready"' angezeigt hat und somit die Daten aufnehmen wird, wird die Adresse inkrementiert.
Dies wird fortgeführt bis alle 2048 Speicherstellen an den IP-Core übertragen sind.
An der letzten Stelle wird zusätzlich das Signal "`s\_axis\_data\_tlast"' auf 1 gesetzt, um dem IP-Core anzuzeigen, dass alle Daten des aktuellen Frames übertragen sind.

Anschließend wird in den Zustand "`WAITING"' gewechselt.
In diesem Zustand führt der IP-Core die Fourier-Transformation aus, während der Zustandsautomat auf das Ende der Berechnungen wartet.
Der IP-Core zeigt das Ende seiner Berechnungen mit dem Signal "`m\_axis\_data\_tdata"' an, welches zur Fertigstellung 1 wird.
Der Zustandsautomat wechselt in den Zustand "`FFT2RAM"'.

Im Zustand "`FFT2RAM"' werden der Anschluss "`WE"' der BRAM-Speicher auf 1 gesetzt und die transformierten Daten an die erste Speicheradresse geschrieben.
Anschließend inkrementiert die Adresse um eins.
Der IP-Core zeigt mit "`m\_axis\_data\_tlast"' an, dass er das letzte Datenpaar überträgt.
Der Zustandsautomat signalisiert mit dem Signal "`finished"' externen Blöcken, dass die Fourier-Transformation abgeschlossen ist und sich das Ergebnis in den BRAM-Speichern findet.
Außerdem springt der Zustandsautomat zurück in den Zustand "`IDLE"'.