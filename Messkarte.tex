\chapter{Entwicklung einer zweikanaligen Messkarte mit Analogmultiplexer}
\label{sec:Messkarte}
In diesem Kapitel wird die Messkarte entwickelt.
Sie digitalisiert die analogen Signale der Elektrodenarrays und leitet sie an das ZedBoard weiter.
Die schematische Darstellung der Messkarte befindet sich in Abbildung \ref{fig:BSB_Messkarte}.
Die fertig hergestellte Messkarte befindet sich in Abbildung \ref{fig:fertige Messkarte}.
Der vollständige Schaltplan der Messkarte ist in Anhang \ref{sec:Schaltplan} angehangen.
Im Anhang \ref{sec:Platinenlayout} findet sich das Platinenlayout.
Die Messkarte wird über einen \ac{FMC}-Stecker mit dem ZedBoard verbunden.

Auf der Messkarte lassen sich je 16 Elektroden zum Emittieren und Sensieren von elektrischen Feldern anschließen.
Die Spannungsversorgung erfolgt über einen Anschluss für $\pm$\SI{4.2}{\volt}. Weitere notwendige Spannungen erzeugt sich die Messkarte selbst.

\begin{figure}[ht]
	\input{BSB_Messkarte}
	\caption[Schematischer Aufbau der Messkarte]{
	Schematischer Aufbau der Messkarte.
	Über die \ac{MMCX}-Stecker SIG1 und SIG2 können Signale auf die Messkarte gegeben werden.
	Diese werden über die Multiplexer 3 und 4 auf eine der emittierenden Elektroden $E_1$ bis $E_{16}$ weitergeleitet.
	Außerdem werden die Signale über einen Verstärker an einen AD-Wandler geleitet, an dem sie digitalisiert werden.
	Das von den Elektroden $E_1$ bis $E_{16}$ emittierte Signal kann über die sensierenden Elektroden $S_1$ bis $S_{16}$ gemessen werden.
	Die Multiplexer 1 und 2 selektieren je eine Elektrode und leiten das Signal an einen Differenzverstärker weiter.
	Dieser hat eine einstellbare Verstärkung.
	Die verstärkten Signaldifferenzen werden vom ADC digitalisiert.
	Der AD-Wandler ist über einen sogenannten \ac{FMC}-Stecker mit dem ZedBoard und dem drauf enthaltenem \ac{SoC} verbunden.
	}
	\label{fig:BSB_Messkarte}
\end{figure}

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{Abbildungen/Platine.jpg}
	\caption[Hergestellte Messkarte]{
	Die fertig hergestellte Messkarte.
	Rechts befinden sich die 16 Anschlüsse sensierende Elektodenarray, mittig die Anschlüsse für das emittierende Eletrodedenarray.
	Der \ac{FMC}-Stecker zum Anschluss des ZedBoards befindet sich links auf der Unterseite der Platine.
	Die langen Sechskantschrauben dienen als Abstandshalter.
	}
	\label{fig:fertige Messkarte}
\end{figure}


Das zu emittierende Signal wird über die \acf{MMCX}-Stecker SIG1 und SIG2 in die Messkarte eingespeist.
Über die Multiplexer 3 und 4 wird das Signal an zwei frei wählbare \ac{MMCX}-Stecker weitergeleitet.
An diese können die emittierenden Elektroden $E_1$ bis $E_{16}$ angeschlossen werden.
Außerdem werden die eingespeisten Signale über einen Verstärker an einen AD-Wandler weitergeleitet.

Die sensierenden Elektroden $S_1$ bis $S_{16}$ können ebenfalls über \ac{MMCX}-Stecker an die Messkarte angeschlossen werden.
Um die Störanfälligkeit der Signale zu verringern, erfolgt unmittelbar darauf je ein Impedanzwandler pro Kanal.
Durch die Multiplexer 1 und 2 kann je ein Signal zum Messen ausgewählt werden.
Die selektierten Signale werden über einen einstellbaren Differenzverstärker verstärkt.
Dadurch ist es möglich sowohl kleine als auch große Signaldifferenzen mit dem folgenden AD-Wandler möglichst optimal zu erfassen.

\section{Analogmultiplexer}
\label{sec:Multiplexer}
Auf der Messkarte werden Analogmultiplexer eingesetzt, um ein Signal auf eine von mehreren emittierenden Elektroden zu verteilen, aus mehreren sensierenden Elektroden eine Elektrode auszuwählen und um verschiedene Widerstände für eine variable Verstärkung auszuwählen.
Es handelt sich in allen Fällen um eine 1:N oder eine N:1 Schaltungskonfiguration.
Da Analogmultiplexer bidirektional funktionieren, kann für alle Aufgaben der gleiche Analogmultiplexer eingesetzt werden.

Wichtig bei den Aufgaben ist, dass sich die Kanäle untereinander möglichst nicht negativ beeinflussen.
Der gegenseitige Einfluss wird Crosstalk genannt und gemäß Gleichung \eqref{eq:Crosstalk} bestimmt.
Dieser muss für den Anwendungsfall dieser Arbeit möglichst gering sein.
Der Analogmultiplexer Hersteller misst den Crosstalk gemäß Abbildung \ref{fig:Crosstalk Messaufbau}.
Ein Kanal wird angesteuert und die dort anliegende Spannung $V_{OUT}$ gemessen, während die restlichen Kanäle gebrückt werden und eine Störspannung $V_S$ angelegt wird.

\begin{equation} \label{eq:Crosstalk}
\text{CROSSTALK} =20 \log\frac{V_{OUT}}{V_S}
\end{equation}

\begin{figure}[ht]
	\centering
	\subcaptionbox{\label{fig:Crosstalk Messaufbau}}
	[.49\textwidth]{\includegraphics{Abbildungen/ADG706_Crosstalk_Messaufbau.eps}}
	\subcaptionbox{\label{fig:R_ON}}
	[.49\textwidth]{\includegraphics{Abbildungen/ADG1414_RON_ueber_Supply_V.eps}}
	\caption[Crosstalk bei Analogmultiplexer \& Spannungsabhängigkeit von $R_{ON}$]
	{
	\subref{fig:Crosstalk Messaufbau} zeigt den Messaufbau zur Bestimmung des Crosstalks.
	Ein Kanal wird ausgewählt und die dort anliegende Spannung $V_{OUT}$ gemessen.
	Alle weiteren Kanäle sind gebrückt und mit einer Spannung $V_S$ beaufschlagt.
	Der Crosstalk lässt sich gemäß Gleichung \eqref{eq:Crosstalk} ermitteln.
	(aus \cite[S. 9]{ADG706})
	
	\subref{fig:R_ON} zeigt die Abhängigkeit des Durchgangswiderstandes $R_{ON}$.
	Dieser ist abhängig von den Betriebsspannungen  $V_{DD}$ und $V_{SS}$, aber auch von der Spannung des Signales($V_S$) selbst.
	(aus \cite[S. 12]{ADG1414})
	}
	\label{fig:Analogmultiplexer}
\end{figure}

Für die Multiplexer 3 und 4, die für die emittierenden Elektroden eingesetzt werden, ist ein möglichst geringer Durchgangswiderstand $R_{ON}$ wichtig.
Um unabhängig vom ausgewählten Multiplexerkanal gleiche Ergebnisse zu erzielen, ist es wichtig, dass die Widerstandsunterschiede zwischen den Kanälen $\Delta R_{ON}$ klein sind.
Der Durchgangswiderstand hängt von der Versorgungsspannung, aber auch von der Spannung des durchzuleitenden Signals ab.
Dadurch entstehen Verzerrungen.
Diese Abhängigkeit ist in Abbildung \ref{fig:R_ON} dargestellt.
$R_{FLAT(ON)}$ gibt an, wie groß diese Art von Widerstandsänderung ist.
Ein kleinerer Wert sorgt für geringere Verzerrungen.

In Tabelle \ref{tab:Multiplexer_Auswahl} sind die Parameter einiger Multiplexer aufgeführt.
Zwar verfügt der ADG714 über bessere Crosstalk Eigenschaften, jedoch hat der ADG706 16 Kanäle und erlaubt so größere Elektrodenarrays.
Die Ansteuerung kann parallel erfolgen.
Dies ist einfach auf dem \ac{SoC} zu implementieren.
Die maximale Betriebsspannung liegt bei $\pm$ \SI{3.3}{\volt} und ist somit auch mit der Betriebsspannung des AD-Wandlers aus Abschnitt \ref{sec:AD-Wandler} kompatibel.

\begin{table}[ht!]
\centering
		\begin{tabular}{|l|r|r|r|r|r|r|}
			 \hline
\textbf{Typ} 					& \textbf{Kanäle}	& \boldmath{$U_{B}$}	& \textbf{Crosstalk}	& \boldmath{$R_{ON}$}	& \boldmath{$R_{FLAT(ON)}$} & \boldmath{$\Delta R_{ON}$} \\ \hline
ADG608 \cite{ADG608}		&  8 						& \SI{5}{\volt}				& \SI{-65}{dB}				& \SI{40}{\ohm}			& \SI{2}{\ohm}						& \SI{5}{\ohm}    \\ \hline
ADG1606 \cite{ADG1606}	& 16 						& \SI{5}{\volt}				& \SI{-62}{dB}				& \SI{8.5}{\ohm}		& \SI{1.8}{\ohm}					& \SI{0.3}{\ohm}  \\ \hline
ADG714 \cite{ADG714}		&  8 						& $\pm$\SI{2.5}{\volt}& \SI{-90}{dB}				& \SI{2.5}{\ohm}		& \SI{0.6}{\ohm}					& \SI{0.4}{\ohm}  \\ \hline
ADG1608 \cite{ADG1608}	&  8 						& \SI{5}{\volt}				& \SI{-64}{dB}				& \SI{8.5}{\ohm}		& \SI{1.7}{\ohm}					& \SI{0.15}{\ohm} \\ \hline
ADG732 \cite{ADG732}		& 16 						& $\pm$\SI{2.5}{\volt}& \SI{-72}{dB}				& \SI{4}{\ohm}			& \SI{0.5}{\ohm}					&	\SI{0.3}{\ohm}  \\ \hline
ADG706 \cite{ADG706}		& 16 						& $\pm$\SI{2.5}{\volt}& \SI{-80}{dB}				& \SI{2.5}{\ohm}		& \SI{0.5}{\ohm}					&	\SI{0.3}{\ohm}  \\ \hline
ADG708 \cite{ADG708}		&  8 						& $\pm$\SI{2.5}{\volt}& \SI{-80}{dB}				& \SI{2.5}{\ohm}		& \SI{0.6}{\ohm}					& \SI{0.4}{\ohm}  \\ \hline
DG9408 \cite{DG9408}		&  8 						& $\pm$\SI{3}{\volt}	& \SI{-85}{dB}				& \SI{12}{\ohm}			& \SI{13}{\ohm}						& \SI{3.6}{\ohm}  \\ \hline
		\end{tabular}
		\caption[Eigenschaften von möglichen Multiplexern]{
		Multiplexer und deren wichtigsten Eigenschaften für den den vorgesehen Anwendungsfall.
		Der ADG706 bietet mit Ausnahme des Crosstalks überall die besten Eigenschaften.
		}
		\label{tab:Multiplexer_Auswahl}
\end{table}
 

\section{Reduzierung des Crosstalks}
\label{sec:Guarding}
Die von den Elektroden sensierten Signale haben eine durch das Wasser vorgegebene, hohe Impedanz und nur eine geringe Amplitude.
Benachbarte Signale können sich gegenseitig beeinflussen.
Außerdem können externe Störungen das Signal zusätzlich beeinflussen. 
Daher werden geschirmte Anschlüsse verwendet.
Die \ac{MMCX}-Anschlüsse sind koaxial aufgebaut und ein äußerer Schirm schützt das eigentliche Nutzsignal im Inneren, außerdem sind im Vergleich zu anderen koaxialen Anschlüssen relativ klein.
Auch die verwendeten Kabel zwischen Signalerzeuger und der Messkarte sind geschirmt.

Die Signale des sensierenden Elektrodenarrays werden unmittelbar neben den Anschluss an einen Impedanzwandler angeschlossen.
Dieser erzeugt ein niederohmiges Signal am Ausgang, welches störunempfindlicher ist.

Zwischen \ac{MMCX}-Stecker und Impedanzwandler sind einzelne Leiterbahnen von sogenannt Guarding-Flächen umgeben.
Abbildung \ref{fig:Guarding} zeigt die Guarding-Flächen der Elektrodenanschlüsse $S_1$ und $S_2$.
Diese Fläche kann wahlweise an Masse oder an dem niederimpedanten, rückgekoppelten Signal angeschlossen werden. So sollen äußere Störeinflüsse minimiert werden.

\begin{figure}[ht!]
	\includegraphics[width=\textwidth]{Abbildungen/Guarding.png}
	\caption[Anschlussmöglichkeiten der Guarding-Flächen]{
	Die Leiterbahn (dunkelrot) zwischen zwei \ac{MMCX}-Steckern (links, schwarz) und dem Impedanzwandler (rechts, schwarz) wird von einer Guarding-Fläche (blau) umgeben.
	Die Guarding-Fläche kann wahlweise mit Masse (gelb) oder dem rückgekoppelten Signal (hellrot) verbunden werden.
	So sollen Störeinfüsse vermieden werden.
	}
	\label{fig:Guarding}
\end{figure}
Als  Impedanzwandler wurde der AD8664 der Firma Analog Devices gewählt.
Er verfügt über vier Operationsverstärker pro Chip.
Somit werden vier Chips benötigt, um alle Elektroden mit einem eigenen Impedanzwandler auszustatten.
Für hochimpedante Signale eignet er sich besonders, da er nur einen geringen Eingangsstrom von max. \SI{1}{\pico\ampere} aufweist.
Auch ist die Offset-Spannung mit \SI{100}{\micro\volt} sehr gering.
Die Bandbreite von \SI{4}{\mega\hertz} ist für Signale von \SI{1}{\mega\hertz} ebenfalls ausreichend.


\section{Verstärker}
\label{sec:Verstärker}
Bevor die Potentialdifferenzen mit dem AD-Wandler digitalisiert werden können, müssen die Potentiale von selektierten Elektroden gebildet werden.
Dazu wird ein diskret aufgebauter Instrumenten Verstärker eingesetzt.
Der Verstärker für die sensierenden Signale verfügt zusätzlich über einen Multiplexer, über den 16 verschiedene Stufen eingestellt werden können.
Der Schaltplan dieses Verstärkers ist in Abbildung \ref{fig:Verstärker} dargestellt.
Der Ausgang des Instrumentenverstärkers ist differenziell, das heißt, die Ausgangsspannung $U_A$ bezieht sich nicht auf Masse, sondern ist die Differenz der positiven und negativen Spannung $U_{A} = U_{A+} - U_{A-}$.

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{Abbildungen/Ausschnitt_Verstaerker.eps}
	\caption[Schaltplan des stufenweise einstellbarer Verstärkers]{
	Schaltplan zum stufenweise einstellbaren Verstärker. Über die Anschlüsse "`MUX1"' und "`MUX2"' sind die Multiplexer 1 und 2 mit dem Verstärker verbunden.
	Über den Multiplexer "`U3"' lässt sich die Verstärkung einstellen. Bei dem Das Verstärkte Signal wird an den Anschlüssen "`SIG\_A\_P"' "`SIG\_A\_M"' ausgeben.
	}
	\label{fig:Verstärker}
\end{figure}

Die Verstärkung $V$ der Eingangsdifferenz $U_E = U_{MUX1} - U_{MUX2}$ ist von den verwendeten Widerständen abhängig und berechnet sich nach Gleichung \eqref{eq:V_berechnen}.
Die Formel setzt voraus, dass R10, R16, R21 und R17 wertgleich sind und somit keinen Einfluss auf die Verstärkung haben.
Ebenfalls müssen R19 und R37 wertgleich sein.
Sie wurden in der Formel mit R substituiert.
$R_{V}$ repräsentiert den vom Multiplexer selektierten Widerstand.

\begin{equation} \label{eq:V_berechnen}
V = \frac{U_A}{U_E}=\left( 1+ \frac{2R}{R_{V}}\right)
\end{equation}

Bei dem zur Selektion der Widerstände genutzten Multiplexer handelt es sich um den ADG706.
Dieser verfügt über 16 Stufen.
Seine Eigenschaften sind in Abschnitt \ref{sec:Multiplexer} beschrieben.
Als minimale Verstärkung wurde 1000 festgelegt.
Die Stufen wurden exponentiell nach Gleichung \eqref{eq:Verstärkung} festgelegt.
$N$ ist die Anzahl der vorhandenen Stufen, $n$ die aktuelle Stufe und $V_{max}$ die maximale Verstärkung.

\begin{equation} \label{eq:Verstärkung}
V(n) = {V_{max}}^{\frac{n}{N}}
\end{equation}

Durch Einsetzen und Umstellen von Gleichung \eqref{eq:V_berechnen} und \eqref{eq:Verstärkung} ergibt sich Gleichung \eqref{eq:V_Widerstände}.
Mit dieser Gleichung kann der notwendige Widerstand bestimmt werden, um die gewünschte Verstärkung zu erreichen.

Allerdings lassen sich Widerstände nicht in beliebigen Widerstandswerten kaufen.
Sie folgen sogenannten E-Reihen.
Für die in der Schaltung verwendeten Widerstandswerte wurden Widerstände aus der E48 Reihe ausgewählt.
Dabei wurde der Widerstandswert immer so gewählt, dass der Verstärkungsfehler möglichst gering ist.
Tabelle \ref{tab:Widerstandswerte Verstärkung} führt die Verstärkungsstufen, die ideal dafür notwendigen Widerstandswerte, die nächst passenden Widerstandswerte und die daraus tatsächlich resultierenden Verstärkungen auf.

\begin{equation} \label{eq:V_Widerstände}
R_V(n) = \frac{2R}{{V_{max}}^{\frac{n}{N}}-1}
\end{equation}

\begin{table}[ht]
	\centering
		\begin{tabular}{|r|r|r|r|r|}
			\hline
			Stufe	&	$V_{ideal}$	&	$R_{V(ideal)}$					&	$R_{V(\text{bestückt})}$			&	$V_{\text{bestückt}}$	\\
			\hline
			1			&	1,54				& \SI{711.21}{\kilo\ohm}	& \SI{715}{\kilo\ohm}		&	1,54						\\
			\hline
			2			&	2,37				& \SI{280.01}{\kilo\ohm}	& \SI{274}{\kilo\ohm}		&	2,40						\\
			\hline
			3			&	3,65				& \SI{144.81}{\kilo\ohm}	& \SI{147}{\kilo\ohm}		&	3,61						\\
			\hline
			4			&	5,62				& \SI{830.56}{\kilo\ohm}	& \SI{82.5}{\kilo\ohm}	&	5,65						\\
			\hline
			5			&	8,66				& \SI{50.13}{\kilo\ohm}		& \SI{51.1}{\kilo\ohm}	&	8,51						\\
			\hline
			6			&	13,34				& \SI{31.13}{\kilo\ohm}		& \SI{31.6}{\kilo\ohm}	&	13,15						\\
			\hline
			7			&	20,54				& \SI{19.66}{\kilo\ohm}		& \SI{19.6}{\kilo\ohm}	&	20,59						\\
			\hline
			8			&	31,62				& \SI{12.54}{\kilo\ohm}		& \SI{12.7}{\kilo\ohm}	&	31,24						\\
			\hline
			9			&	48,70				& \SI{8.05}{\kilo\ohm}		& \SI{7.87}{\kilo\ohm}	&	49,79						\\
			\hline
			10		&	74,99				& \SI{5.19}{\kilo\ohm}		& \SI{5.11}{\kilo\ohm}	&	76,15						\\
			\hline
			11		&	115,48			& \SI{3.35}{\kilo\ohm}		& \SI{3.32}{\kilo\ohm}	&	116,66					\\
			\hline
			12		&	177,83			& \SI{2.17}{\kilo\ohm}		& \SI{2.15}{\kilo\ohm}	&	179,60					\\
			\hline
			13		&	273,84			& \SI{1.41}{\kilo\ohm}		& \SI{1.4}{\kilo\ohm}		&	275,29					\\
			\hline
			14		&	421,70			& \SI{912.77}{\ohm}				& \SI{909}{\ohm}				&	423,44					\\
			\hline
			15		&	649,38			& \SI{592.24}{\ohm}				& \SI{590}{\ohm}				&	651,85					\\
			\hline
			16		&	1000				& \SI{384.38}{\ohm}				& \SI{383}{\ohm}				&	1003,61					\\
			\hline
		\end{tabular}
	\caption[Verstärkungsstufen der Messkarte]{
		Die Verstärkungstufen der Messkarte wurden nach Gleichung \eqref{eq:Verstärkung} berechnet.
		Die maximale Verstärkung liegt bei 1000 und die minmale bei 1,54.
		Die für diese Verstärkungen notwendigen Widerstandswerte berechnen sich nach Gleichung \eqref{eq:V_Widerstände}.
		Da Widerstände nur mit bestimmten Werten angeboten werden, wurde der nächst passende Widerstand ausgewählt und bestückt.
		Dadurch kommt es zu geringen Abweichungen von der ursprünglich festgelegten Verstärkung.
	}
	\label{tab:Widerstandswerte Verstärkung}
\end{table}

Der Verstärker für die Signale der emittierenden Elektroden ist nahezu identisch.
Statt eines Multiplexers mit verschiedenen Widerständen ist nur ein einfacher Widerstand vorgesehen.
Dieser ist jedoch nicht bestückt, somit ideal unendlich groß.
Für die Verstärkung ergibt sich nach Gleichung \eqref{eq:Verstärkung} ein Wert von 1.

\section{AD-Wandler}
\label{sec:AD-Wandler}
Nach der Verstärkung der Signale erfolgt die Wandlung der analogen Signale in digitale Signale.
Zu diesem Zweck wird der ADS62P45 von Texas Instruments verwendet.
Dieser wurde bereits in anderen Arbeiten verwendet \cite{Kus2015}.
Der ADS62P45 verfügt über zwei Kanäle mit jeweils einer Auflösung von 14 Bit.
Jeder Kanal wird dabei differenziell gemessen.
Zur Entkopplung zwischen Verstärker und AD-Wandler wird die vom Datenblatt (\cite[S. 50]{ADS62P45}) empfohlene Schaltung verwendet.
Als Taktquelle dient der AD9573 der Firma Analog Devices.
Er ist so konfiguriert, dass er mit \SI{100}{\mega\hertz} taktet.
Der AD-Wandler misst pro Takt einmal, so dass die Samplingrate ebenfalls \SI{100}{\mega\hertz} entspricht.

Die Ausgabe erfolgt über eine parallele Schnittstelle vom Typ \ac{LVDS}.
\ac{LVDS} eignet sich besonders gut für Hochgeschwindigkeitsübertragungen.
Im Gegensatz zur ebenfalls vorhandenen \ac{CMOS}-Schnittstelle werden die Daten differenziell übertragen, somit sind zur Übertragung eines Signals zwei Leitungen notwendig.
Allerdings ist \ac{LVDS} unanfälliger gegenüber Störungen.
Die Spannungspegel der beiden Leitungen sind gering.
Der Spannungshub beträgt nur \SI{350}{\milli\volt}.
Die Signale werden nicht über eine Spannungsquelle, sondern über eine Stromquelle erzeugt.
Die Stromquelle erzeugt für jedes Leitungspaar einen konstanten Strom von \SI{3.5}{\milli\ampere}.
Über einen \SI{100}{\ohm} Widerstand auf der Empfängerseite, in diesem Fall auf dem \ac{FPGA}, wird der Spannungspegel erzeugt, der auch direkt beim Empfänger ausgewertet wird.
Durch die stromgesteuerte, differenzielle Datenübertragung können trotz geringer Spannung hohe Datenraten störungssicher übertragen werden.
Auch Störaussendungen werden vermieden, wenn die korrespondierenden Leitungen nah aneinander liegen.
Zwei 14 Bit differenziell und parallele übertragene Zahlenwerte würden insgesamt 56 Leitungen benötigen.
Um Leitungen einzusparen und auch die Baugröße des Chips gering zu halten, werden die Daten mit der doppelten Rate (\ac{DDR}) übertragen. Nutzdaten liegen hierbei nicht nur bei steigender Flanke des Taktsignals, sondern auch bei fallender Flanke an. Hierdurch reduzieren sich die nötigen Leitungen auf 28.

\section{Weitere Hardware}
Neben der selbst entworfenen und hergestellten Messkarte wird weitere Hardware benötigt und mit der Messkarte verbunden.
Abschnitt \ref{sec:zedboard} stellt das ZedBoard vor und Abschnitt \ref{sec:MSOX} das Oszilloskop MSO-X 4054, welches zur Messung aber auch Signalerzeugung eingesetzt wird.
Außerdem wird ein Labornetzteil PL303QMD-P der Firma \ac{TTi} zur Bereitstellung der Versorgungsspannung von $\pm$ \SI{3.3}{\volt} verwendet.

\subsection{ZedBoard}
\label{sec:zedboard}
Das ZedBoard ist ein von der Firma Avnet hergestelltes Evaluierungsboard.
Es umfasst einen Zynq XC7Z020-Chip der Firma Xilinx.
Hierbei handelt es sich um einen \ac{SoC}, auf dem neben einem \ac{FPGA} auch zwei ARM Cortex Prozessoren enthalten sind.
Neben dem Chip und der notwendigen Spannungsversorgung sind auf dem ZedBoard auch ein DDR3 Speicher, OLED-Display, Taster und LED vorhanden.
Außerdem verfügt es über vielfältige Anschlüsse für Netzwerk, sowie Video- und Audio-Geräte.
Über einen \ac{FMC}-Stecker und sogenannte PMod-Stecker können Erweiterungssplatinen angeschlossen werden.
Die Programmierung erfolgt über einen USB-Anschluss.

Innerhalb dieser Arbeit wird das ZedBoard über die serielle Schnittstelle gesteuert und über den FMC-Stecker mit der Messkarte verbunden.
Über diesen werden die Multiplexer adressiert und der AD-Wandler ausgelesen.

\subsection{MSO-X 4054}
\label{sec:MSOX}
Das Oszilloskop Agilent Technologies InfiniiVision MSO-X 4054 dient zum Erzeugen und Messen der Signale für bzw. auf der Messkarte.
Die zwei integrierten Funktionsgeneratoren können Signale unterschiedlicher Wellenform erzeugen.
Der Frequenzbereich reicht dabei von \SI{100}{\milli\hertz} bis \SI{20}{\mega\hertz} und die Amplitude von \SI{10}{\milli\volt} bis \SI{10}{\volt}. Die Funktionsgeneratoren werden genutzt um das zu emittierende Signal zu erzeugen.

Das Oszilloskop verfügt über vier analoge Messkanäle. Diese messen über eine Bandbreite von \SI{500}{\mega\hertz} und können mit einer Abtastrate von bis zu \SI{5}{\giga\sample\per\second} betrieben werden.

Die Ansteuerung des Oszilloskops erfolgt über USB und den standardisierten Befehlssatz \ac{SCPI}.